﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    interface INetworkPacket
    {
        #region Methods
         /// <summary>
        /// Encode all the class data into a buffer.
        /// </summary>
        void Encode();

        /// <summary>
        /// Decode a buffer into class data.
        /// </summary>
        /// <param name="buffer"></param>
        void Decode(byte[] buffer);

        /// <summary>
        /// Called when this packet is received.
        /// </summary>
        void HandleReceive();

        byte[] getBuffer();

        int getPacketSize();
        #endregion
    }
}
