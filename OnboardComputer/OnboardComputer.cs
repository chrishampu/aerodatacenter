﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Network;
using Network.Packets;

namespace OnboardComputer
{
    class OnboardComputer
    {
       static public NetworkPlatform Net;
       static private ImageCapture Images;

        static void Main(string[] args)
        {
            Net = new NetworkPlatform();
            Net.Connect();

            Images = new ImageCapture();

            while(true)
            {
                Net.Process();
            }
        }
    }
}
