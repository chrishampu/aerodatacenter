﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Network.Packets;
using OnboardComputer;

using FlyCapture2Managed;

namespace OnboardComputer
{
    class ImageCapture
    {
        #region Constructors/Destructors
        public ImageCapture()
        {
            SetupCamera();
        }
        
        ~ImageCapture()
        {
            Camera.StopCapture();

            Camera.Disconnect();
        }
        #endregion

        #region Properties
        private ManagedCamera Camera;
        private int CaptureCount = 0;
//        private CameraProperty FrameRate;
        private static Mutex mutex = new Mutex();
        #endregion

        #region Methods
        public void SetupCamera()
        {
            ManagedBusManager busMgr = new ManagedBusManager();

            if(busMgr.GetNumOfCameras() == 0)
            {
                Console.WriteLine("No camera detected");
                return;
            }

            ManagedPGRGuid guid = busMgr.GetCameraFromIndex(0);

            Camera = new ManagedCamera();

            Camera.Connect(guid);

            EmbeddedImageInfo embeddedInfo = Camera.GetEmbeddedImageInfo();

            // Enable timestamp collection in case we decide to use it
            if (embeddedInfo.timestamp.available == true)
                embeddedInfo.timestamp.onOff = true;

            Camera.SetEmbeddedImageInfo(embeddedInfo);

            Thread thread = new Thread(new ThreadStart(ImageWorker));
            thread.Start();
        }

        private void SendImagePacket(System.Drawing.Bitmap bitmap, byte[] data)
        {
            PacketAerialImage packet = new PacketAerialImage((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds, 0, 
                0, 0, 0, 0, 0, 0, data.Length, 0, 0, 0, 0, bitmap.Width, bitmap.Height, data);

            packet.Encode();

            OnboardComputer.Net.SendPacket(packet);
        }

        private void SendFakeImagePacket(byte[] data)
        {
            PacketAerialImage packet = new PacketAerialImage((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds, 0,
                0, 0, 0, 0, 0, 0, data.Length, 0, 0, 0, 0, 1920, 1080, data);

            packet.Encode();

            OnboardComputer.Net.SendPacket(packet);
        }

        private void ImageWorker()
        {
           Camera.StartCapture();

            while(true)
            {
                mutex.WaitOne();

                ManagedImage raw = new ManagedImage();
                ManagedImage convertedImage = new ManagedImage();

                Camera.RetrieveBuffer(raw);
                  
                raw.Convert(PixelFormat.PixelFormatBgr, convertedImage);

                System.Drawing.Bitmap bmp = convertedImage.bitmap;

                System.Drawing.Imaging.BitmapData bData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);

                int size = bData.Height * bData.Width * 3;
                byte[] rgbValues = new byte[size];

                unsafe
                {
                    byte* scan0 = (byte*)bData.Scan0.ToPointer();
                    IntPtr dstPtr = System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement(rgbValues, 0);
                    byte* dst = (byte*)dstPtr.ToPointer();

                    for (int n = 0; n < size; n++)
                    {
                        *dst = *scan0;
                        dst++;
                        scan0++;
                    }
                }

                bmp.UnlockBits(bData);

                SendImagePacket(bmp, rgbValues);

                CaptureCount++;

                mutex.ReleaseMutex();

                Thread.Sleep(1000);

                // Due to an anomaly in the Camera libraryw, the capture must be reset every 10 images
                if(CaptureCount % 10 == 0)
                {
                    Camera.StopCapture();
                    Camera.StartCapture();
                }
            }
        }
        #endregion
    }
}
