﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    public abstract class NetworkPacket
    {
        #region Properties
        protected PacketIDEnum PacketID;

        //static int PacketBufferSize = 1024;

        protected byte[] PacketBuffer;
        protected int PacketSize;

        /// <summary>
        /// List of valid packet IDs.
        /// </summary>
        public enum PacketIDEnum
        {
            PacketAerialImageID = 0x106
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get the packet buffer associated to this class.
        /// </summary>
        /// <returns>Packet buffer</returns>
        public byte[] getBuffer()
        {
            return PacketBuffer;
        }

        /// <summary>
        /// Get actual size of packet buffer.
        /// </summary>
        /// <returns>Packet size</returns>
        public int getPacketSize()
        {
            return PacketSize;
        }
        #endregion
    }
}
