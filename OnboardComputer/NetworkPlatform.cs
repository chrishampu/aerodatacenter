﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Network.Packets;

namespace Network
{
    class NetworkPlatform : IDisposable
    {
        #region Callbacks
        private void ConnectCallback(IAsyncResult result)
        {
            Socket client = (Socket)result.AsyncState;

            try
            {
                client.EndConnect(result);

                Console.WriteLine("Connection established to {0}", client.RemoteEndPoint.ToString());


            }
            catch(SocketException)
            {
                Console.WriteLine("Failed to connect to server");
            }
        }

        private void ReadCallback(IAsyncResult result)
        {
            Socket sock = (Socket)result.AsyncState;

            int bytesRead = sock.EndReceive(result);

            if(bytesRead > 0)
            {
                Console.WriteLine("Data received: {0}", Buffer);
            }
        }

        private void WriteCallback(IAsyncResult result)
        {
            Socket sock = (Socket)result.AsyncState;

            Console.WriteLine("Data sent: {0}", sock.EndSend(result));
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>
        /// Instantiate class and create TCP object.
        /// </summary>
        public NetworkPlatform()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        ~NetworkPlatform()
        {

        }
        #endregion

        #region Properties
        public static IPAddress ServerAddress = IPAddress.Parse("192.168.0.10");
        public static int ServerPort = 24000;

        private Socket Client;
        private byte[] Buffer = new byte[1024];
        #endregion

        #region Methods
        /// <summary>
        /// Connect to the server.
        /// </summary>
        public void Connect()
        {
            Client.BeginConnect(ServerAddress, ServerPort, new AsyncCallback(ConnectCallback), Client);
        }

        /// <summary>
        /// Process function will check if there is any incoming data to be read.
        /// </summary>
        public void Process()
        {
            if(Client.Connected == true)
            {
                if(Client.Available > 0)
                {
                    Client.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, ReadCallback, Client);
                }
            }
        }

        /// <summary>
        /// Send a packet to the connected server.
        /// </summary>
        /// <param name="buffer">Packet buffer to send</param>
        public void SendPacket(INetworkPacket packet)
        {
            if (Client.Connected == true)
            {
                Client.BeginSend(packet.getBuffer(), 0, packet.getBuffer().Length, SocketFlags.None, WriteCallback, Client);
            }
        }

        /// <summary>
        /// Clean up resources.
        /// </summary>
        public void Dispose()
        {
            Client.Close();
        }
        #endregion
    }
}
