﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Network;

namespace Network.Packets
{
    class PacketAerialImage : NetworkPacket, INetworkPacket
    {
        #region Constructors/Destructors
        /// <summary>
        /// Construct an AerialImage packet given all the necessary parameters.
        /// </summary>
        public PacketAerialImage( int timestamp, int imageID, float latitude, float longitude, float attitude, float heading, float pitchAngle, float rollAngle, int urlLength, float maxLon,
	                         float minLat, float minLon, float maxLat, int width, int height, byte[] url)
        {
            this.PacketID = PacketIDEnum.PacketAerialImageID;
            this.timestamp = timestamp;
            this.imageID = imageID;
            this.latitude = latitude;
            this.longitude = longitude;
            this.attitude = attitude;
            this.heading = heading;
            this.pitchAngle = pitchAngle;
            this.rollAngle = rollAngle;
            this.urlLength = urlLength;
            this.maxLon = maxLon;
            this.minLat = minLat;
            this.minLon = minLon;
            this.maxLat = maxLat;
            this.width = width;
            this.height = height;
            this.url = url;
        }

        ~PacketAerialImage()
        {

        }
        #endregion

        #region Properties
	    int timestamp;
	    int imageID;
	    float latitude;
	    float longitude;
	    float attitude;
	    float heading;
	    float pitchAngle;
	    float rollAngle;
	    int urlLength;
	    float maxLon;
	    float minLat;
	    float minLon;
	    float maxLat;
	    int width;
	    int height;
	    byte[] url;
        #endregion

        #region Methods
        public void Encode()
        {
            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write((short)PacketID);
	            writer.Write(timestamp);
	            writer.Write(imageID);
	            writer.Write(latitude);
	            writer.Write(longitude);
	            writer.Write(attitude);
	            writer.Write(heading);
	            writer.Write(pitchAngle);
	            writer.Write(rollAngle);
	            writer.Write(urlLength);
	            writer.Write(maxLon);
	            writer.Write(minLat);
	            writer.Write(minLon);
	            writer.Write(maxLat);
	            writer.Write(height); // Because we convert from Microsofts GDI format, the height & width values are reversed
                writer.Write(width);
	            writer.Write(url);

                PacketSize = (int)writer.BaseStream.Length;
            }

            PacketBuffer = stream.GetBuffer();
        }

        // This packet won't be received
        public  void Decode(byte[] buffer)
        {
        
        }

        public void HandleReceive()
        {

        }
        #endregion
    }
}
