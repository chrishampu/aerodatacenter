#include "Query.h"

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK
#endif

QueryResult::QueryResult(MYSQL_RES* res, unsigned int Fields, unsigned int Rows)
	: RowCount(Rows),
	  FieldCount(Fields),
	  CurrentRow(NULL),
	  Result(res)
{
	CurrentRow = new Field[FieldCount];
}

QueryResult::~QueryResult()
{
	mysql_free_result(Result);
	delete [] CurrentRow;
}

void QueryResult::Release()
{
	delete this;
}

bool QueryResult::NextRow()
{
	MYSQL_ROW row = mysql_fetch_row(Result);

	if(row == NULL)
		return false;

	for(unsigned int i = 0; i < FieldCount; ++i)
		CurrentRow[i].SetValue(row[i]);

	return true;
}