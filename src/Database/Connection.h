#pragma once

#include <Common/Platform.h>
#ifdef PLATFORM_WIN
#include <winsock.h>
#endif
#include <mysql/mysql.h>
#include <boost/thread/mutex.hpp>
#include <string>

class Connection
{
public:
	bool Open(std::string host, std::string user, std::string pass, std::string name, unsigned int port);
	void Close();
	void Reconnect();
	void Ping();

	bool isValid() { return MySql != NULL; }
	MYSQL *get() { return MySql; }

	bool TryLock() { return Mutex.try_lock(); }
	void Unlock() { Mutex.unlock(); }
private:
	MYSQL* MySql;
	boost::mutex Mutex;
};