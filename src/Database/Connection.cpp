#include "Connection.h"

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK
#endif

bool Connection::Open(std::string host, std::string user, std::string pass, std::string name, unsigned int port)
{
	MYSQL *temp = mysql_init(NULL);

	if(temp == NULL)
	{
		return false;
	}

	MySql = mysql_real_connect(temp, host.c_str(), user.c_str(), pass.c_str(), 
		name.c_str(), port, NULL, 0);
	if(MySql == NULL)
	{
		mysql_close(temp);

		Console->printf(MySQL, "Failed to establish connection. Possible invalid credentials\n");

		return false;
	}

	mysql_options(MySql, MYSQL_SET_CHARSET_NAME, "utf8");
	mysql_set_character_set(MySql, "utf8");
	mysql_autocommit(MySql, 1);

	Console->printf(MySQL, "Connection to database '%s' established\n", name.c_str());

	return true;
}

void Connection::Close()
{
	mysql_close(MySql);
}

void Connection::Reconnect()
{

}

void Connection::Ping()
{
	mysql_query(MySql, "SELECT 1");
}
