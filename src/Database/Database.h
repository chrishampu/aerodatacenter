#pragma once

#include "Connection.h"
#include "Query.h"

#include <string>
#include <vector>

class Database
{
public:
	Database(std::string host, std::string user, std::string pass, std::string name, int port);
	~Database();

	bool Initialize();
	void Shutdown();

	std::shared_ptr<QueryResult> Query(const char* sql, ...);
	void Execute(const char* sql, ...);

	std::string EscapeString(std::string Escape);

	void KeepAlive();
	bool isConnected() { return Connections.size() > 0; }

private:
	void AddConnection(Connection *con);
	Connection* GetFreeConnection();

	bool SendQuery(Connection *con, const char* sql);
	std::shared_ptr<QueryResult> StoreQueryResult(Connection *con);

	std::string Hostname;
	std::string Username;
	std::string Password;
	std::string DatabaseName;
	int Port;

	typedef std::vector<Connection*> ConnectionVector;
	ConnectionVector Connections;

public:
	typedef ConnectionVector::iterator ConnectionIter;
	typedef ConnectionVector::const_iterator ConnectionConstIter;
};