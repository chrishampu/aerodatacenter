#pragma once

#include "Database.h"
#include <map>

enum DatacenterDatabases {
	Aero = 0
};

class DatabaseManager
{
public:
	DatabaseManager();
	~DatabaseManager();

	void Initialize();
	void Shutdown();

	void AddDatabase(int index, Database *db);
	void RemoveDatabase(int index);

	void KeepAlive();

	Database* operator[](int index);

private:
	typedef std::map<int, Database*> DatabaseMap;
	DatabaseMap mDatabases;

public:
	typedef DatabaseMap::iterator DatabaseIter;
};

extern DatabaseManager Databases;