#include <Common/Platform.h>
#include "Database.h"
#include <cstdarg>

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK
#endif

static int MaxConnections = 2;

Database::Database(std::string host, std::string user, std::string pass, std::string name, int port)
	: Hostname(host),
	  Username(user),
	  Password(pass),
	  DatabaseName(name),
	  Port(port)
{
	if (!Initialize())
		Console->printf(MySQL, "Unable to connect to database %s. Database will be unusable.\n", name.c_str());
}

Database::~Database()
{
	Shutdown();
}

bool Database::Initialize()
{
	for(int i = 0; i < MaxConnections; i++)
	{
		Connection *con;

		if (!((con = new Connection)->Open(Hostname, Username, Password, DatabaseName, Port)))
		{
			Console->printf(MySQL, "Failed to open connection for %s@%s\n", Hostname.c_str(), Username.c_str());
			delete con;
			return false;
		}

		AddConnection(con);
	}

	Console->printf(MySQL, "%d '%s' database connections prepared\n", MaxConnections, DatabaseName.c_str());
	return true;
}

void Database::Shutdown()
{
	ConnectionIter iter;

	for(iter = Connections.begin(); iter != Connections.end();)
	{
		((Connection*)*iter)->Close();
		delete *iter;
		iter = Connections.erase(iter);
	}
}

void Database::AddConnection(Connection *con)
{
	Connections.push_back(con);
}

Connection* Database::GetFreeConnection()
{
	if (Connections.size() <= 0)
		return nullptr;

	Connection *con = nullptr;
	ConnectionIter iter;

	for(;;)
	{
		for(iter = Connections.begin(); iter != Connections.end();)
		{
			con = *iter;

			if(con->TryLock())
				return con;
		}
	}

	return nullptr;
}

std::shared_ptr<QueryResult> Database::Query(const char* sql, ...)
{
	// Get the line into a buffer.
#ifdef _MSC_VER
	static int BufSize = 4096;
	int nSize;
#endif
	static char buffer[4096];
	va_list args;

	va_start(args, sql);

#ifdef _MSC_VER
	nSize = vsnprintf(buffer, sizeof(BufSize), _TRUNCATE, sql, args);
	vsnprintf(buffer, nSize, sql, args);
#else
	vsprintf(buffer, sql, args);
#endif

	if (isConnected() == false)
	{
		Console->printf(MySQL, "Query failed. No available connections: %s\n", buffer);
		return nullptr;
	}

	Connection *con = GetFreeConnection();
	std::shared_ptr<QueryResult> res(nullptr);

	if(con->isValid() == true)
	{
		if( SendQuery(con, buffer) )
			res = StoreQueryResult(con);
	}

	con->Unlock();
	
	return res;
}

void Database::Execute(const char* sql, ...)
{
	// Get the line into a buffer.
#ifdef _MSC_VER
	static int BufSize = 4096;
	int nSize;
#endif
	char buffer[4096];
	va_list args;

	va_start(args, sql);

#ifdef _MSC_VER
	nSize = vsnprintf(buffer, sizeof(BufSize), _TRUNCATE, sql, args);
	vsnprintf(buffer, nSize, sql, args);
#else
	vsprintf(buffer, sql, args);
#endif

	if (isConnected() == false)
	{
		Console->printf(MySQL, "Query failed. No available connections: %s\n", buffer);
		return;
	}

	Connection *con = GetFreeConnection();

	// What a mess. But it works.
	if (con->isValid() == true)
		SendQuery(con, buffer);

	con->Unlock();
}

std::string Database::EscapeString(std::string Escape)
{
	char a2[16384] = { 0 };

	Connection* con = GetFreeConnection();
	std::string ret;

	if(mysql_real_escape_string(con->get(), a2, Escape.c_str(), (unsigned long)Escape.length()) == 0)
		ret = Escape.c_str();
	else
		ret = a2;

	con->Unlock();

	return std::string(ret);
}

void Database::KeepAlive()
{
	ConnectionIter iter;

	for(iter = Connections.begin(); iter != Connections.end();)
	{
		Connection *con = *iter;
		if(con->TryLock())
		{
			con->Ping();
			con->Unlock();
		}
	}

	//Console->printf(MySQL, "Keeping connections alive\n");
}

bool Database::SendQuery(Connection *con, const char* sql)
{
	int result = mysql_query(con->get(), sql);

	if(result > 0 )
		Console->printf(MySQL, "Query exception: '%s'\nFailed query: '%s'\n", mysql_error(con->get()), sql);
	else
		Console->printf(MySQL, "Executed query: %s\n", sql);

	return (result == 0 ? true : false);
}

std::shared_ptr<QueryResult> Database::StoreQueryResult(Connection *con)
{
	QueryResult* res;

	MYSQL_RES* pRes = mysql_store_result(con->get());
	unsigned int uRows = (unsigned int)mysql_affected_rows(con->get());
	unsigned int uFields = (unsigned int)mysql_field_count(con->get());

	if(uRows == 0 || uFields == 0 || pRes == 0)
	{
		if(pRes != nullptr)
			mysql_free_result(pRes);

		return nullptr;
	}

	res = new QueryResult(pRes, uFields, uRows);
	res->NextRow();

	return std::shared_ptr<QueryResult>(res);
}

/*
Connection* Database::operator[](std::string Database)
{
	ConnectionConstIter iter = Connections.find(Database);

	if(iter != Connections.end())
		return Connections[Database];
	else
		return NULL;
}
*/
