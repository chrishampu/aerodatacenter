#include "ImageController.h"
#include "objectrecognizer.h"
#include <Network/NetPlatform.h>
#include <Network/NetPacketManager.h>
#include <Network/NetSessionManager.h>
#include <Network/NetHTTPInterpreter.h>
#include <Database/DatabaseManager.h>
#include <Common/Base64.h>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK
#endif

ImageController *Imaging = NULL;

#ifdef PLATFORM_WIN
AddConsoleFunction(start_simulating, 0, 0)
{
	Imaging->StartSimulating();
}

AddConsoleFunction(stop_simulating, 0, 0)
{
	Imaging->StopSimulating();
}
#endif

ImageController::ImageController(bool simulate)
{
	ImageCount = TargetCount = 0;

	ImageProcessingThread = std::thread(&ImageController::ProcessingLoop, this);

	SimulateLoop = simulate;
	LastSimulation = (unsigned int)std::time(0);

	image_connection = PacketManager->connectPacketReceiver(boost::bind(&ImageController::notifyImageReady, this, _1));

	ProcessThread = true;
}

ImageController::~ImageController()
{
	ProcessThread = false;

	ProcessingQueue.retire();

	ImageProcessingThread.join();

	while (ProcessingQueue.size())
	{
		PacketAerialImage *packet = ProcessingQueue.pop();
		delete packet;
	}

	image_connection.disconnect();
}

void ImageController::notifyImageReady(NetPacket *packet)
{
	switch (packet->GetPacketHeader())
	{
		case pktOnboardImage:
		{
			PacketOnboardImage *imgpacket = reinterpret_cast<PacketOnboardImage*>(packet);

			// Create a copy of the packet
			PacketAerialImage *QueuedPacket = new PacketAerialImage(imgpacket->timestamp, 0, imgpacket->latitude, imgpacket->longitude, imgpacket->altitude, 0,
				imgpacket->pitchAngle, imgpacket->rollAngle, imgpacket->dataLength, 0, 0, 0, 0, imgpacket->width, imgpacket->height, imgpacket->data);

			ProcessingQueue.push(QueuedPacket);

			Console->printf(Networking, "Queued image of size %d for processing\n", QueuedPacket->dataLength);
		}

		case pktAerialImage:
		{
			PacketAerialImage *imgpacket = reinterpret_cast<PacketAerialImage*>(packet);

			// Create a copy of the packet
			PacketAerialImage *QueuedPacket = new PacketAerialImage(imgpacket->timestamp, imgpacket->imageID, imgpacket->latitude, imgpacket->longitude, imgpacket->attitude, imgpacket->heading,
				imgpacket->pitchAngle, imgpacket->rollAngle, imgpacket->dataLength, imgpacket->maxLon, imgpacket->minLat, imgpacket->minLon, imgpacket->maxLat, imgpacket->width, imgpacket->height, imgpacket->data);

			ProcessingQueue.push(QueuedPacket);

			Console->printf(Networking, "Queued image of size %d for processing\n", QueuedPacket->dataLength);
		}
		break;

		case pktImageTarget:
		{
			PacketImageTarget *target = reinterpret_cast<PacketImageTarget*>(packet);

			// A client has given us info about a new target
			if (target->HumanFound == true)
			{
				Console->printf(Debug, "Analyst found target\n");
				int id = TargetCount++;

				PacketImageTarget *reply = new PacketImageTarget(target->imageID, id, target->latitude, target->longitude, target->minX, target->minY, target->maxX,
					target->maxY, 0, false, true, target->jsonLen, target->json);

				NetHTTPInterpreter::EncodeAsFrame(reply, BinaryFrame);
				
				NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();
				for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
					if (itr->second->getSocketType() == WebSocket && itr->second->getSessionFlag(RequiresCameraImages) == true)
						Network->sendtoSocket(itr->second->getSocket(), reply->getPacketBuffer(), reply->getPacketSize());
				
				Databases[Aero]->Execute("REPLACE INTO `aero_datacenter`.`imagetarget` VALUES (%d, %d, %d, '%s', %d, %d, %f, %f)", target->imageID, id,
					std::time(NULL), 0, 1, target->latitude, target->longitude);

				Databases[Aero]->Execute("REPLACE INTO `aero_datacenter`.`target` VALUES (%d, %d, %d)", id, target->latitude, target->longitude);
			}
		}
		break;

		case pktRejectImage:
		{
			PacketRejectImage *image = reinterpret_cast<PacketRejectImage*>(packet);

			// Erase only a specific target if the targeted image ID is invalid
			if (image->ImageID == -1)
			{
				Databases[Aero]->Execute("DELETE FROM `aero_datacenter`.`imagetarget` WHERE `TargetID` = %d", image->TargetID);
				Databases[Aero]->Execute("DELETE FROM `aero_datacenter`.`target` WHERE `TargetID` = %d", image->TargetID);
			}
			else // Otherwise erase all targets/images related to a specific ID
			{
				Databases[Aero]->Execute("DELETE FROM `aero_datacenter`.`aerialimage` WHERE `ID` = %d", image->ImageID);

				// This deletes all entries from the `target` database based on the TargetID's gathered from the `imagetarget` database that match this ImageID
				Databases[Aero]->Execute("DELETE `aero_datacenter`.`target` FROM `aero_datacenter`.`target` INNER JOIN `aero_datacenter`.`imagetarget` ON `aero_datacenter`.`target`.TargetID = `aero_datacenter`.`imagetarget`.TargetID WHERE `aero_datacenter`.`imagetarget`.ImageID = %d", image->ImageID);
				Databases[Aero]->Execute("DELETE FROM `aero_datacenter`.`imagetarget` WHERE `ImageID` = %d", image->ImageID);

			}
		}
		break;

		case pktRecognitionValues:
		{
			PacketRecognitionValues *values = reinterpret_cast<PacketRecognitionValues*>(packet);

			ObjectRecognitionValues.Gaussian = values->gaussian % 2 == 0 ? values->gaussian + 1 : values->gaussian;
			ObjectRecognitionValues.CannyLow = values->cannyLow;
			ObjectRecognitionValues.CannyHigh = values->cannyHigh;
			ObjectRecognitionValues.HoughVote = values->houghVote;
			ObjectRecognitionValues.HoughLength = values->houghLength;
			ObjectRecognitionValues.HoughDistance = values->houghDistance;
		}
		break;

		default:
			return;
	}
}

void ImageController::ProcessingLoop()
{
	while (ProcessThread.load() == true)
	{
		static char timeBuffer[256];
		static time_t rawtime;

		PacketAerialImage *packet = ProcessingQueue.pop();
		if (packet == NULL)
			continue;

		ObjectRecognizer *rec = new ObjectRecognizer();

		rec->gaussianSD			= ObjectRecognitionValues.Gaussian;
		rec->cannyLow			= ObjectRecognitionValues.CannyLow;
		rec->cannyHigh			= ObjectRecognitionValues.CannyHigh;
		rec->houghVote			= ObjectRecognitionValues.HoughVote;
		rec->houghMinLength		= ObjectRecognitionValues.HoughLength;
		rec->houghMinDistance	= ObjectRecognitionValues.HoughDistance;
		rec->imageScale			= ObjectRecognitionValues.ImageScale;
		rec->polyDPError		= ObjectRecognitionValues.PolyDPError;

		const unsigned char *data = reinterpret_cast<const unsigned char*>(packet->data);

		cv::Mat mat = cv::Mat(packet->width, packet->height, CV_8UC3, (unsigned char*)data);

		rec->fullSizeInputImage = mat;

		RecognizerResults *res = rec->recognizeObjects(packet);

		time(&rawtime);

		strftime(timeBuffer, 256, "%Y%m%d_%H%M%S", localtime(&rawtime));

		std::string tag = "Image_" + std::to_string(ImageCount) + "_";
		tag.append(timeBuffer, strlen(timeBuffer));

		std::string out = DocumentRoot;
		out.append("/images/" + tag + ".jpg");

		cv::imwrite(out.c_str(), res->output);

		PacketAerialImage *processedPacket = new PacketAerialImage((unsigned int)std::time(0), ImageCount.load(), 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, tag.length(), 0.f, 0.f, 0.f, 0.f, res->output.rows, res->output.cols, (unsigned char*)tag.c_str());

		NetHTTPInterpreter::EncodeAsFrame(processedPacket, BinaryFrame);

		ProcessedPackets.push(processedPacket);

		// Now process targets
		for (auto i : res->targets)
		{
			auto target = i.get();
			
			PacketImageTarget *imageTarget = new PacketImageTarget(ImageCount.load(), TargetCount.load(), target->latitude, target->longitude, target->boxMinX, target->boxMinY, target->boxMaxX, target->boxMaxY,
																	0, true, false, target->json.size(), target->json.c_str());
			//std::cout << "Target JSON description: " << target->json << std::endl;
			NetHTTPInterpreter::EncodeAsFrame(imageTarget, BinaryFrame);

			ProcessedPackets.push(imageTarget);

			++TargetCount;
		}

		// Clean up memory
		delete packet;
		delete res;
		mat.release();
		delete rec;

		++ImageCount;
	}
}

void ImageController::Update()
{
	if (SimulateLoop == true)
	{
		unsigned int cur = (unsigned int)std::time(0);

		if (cur > LastSimulation)
		{
			cv::Mat img = cv::imread("targets.JPG");

			const unsigned char *data = reinterpret_cast<const unsigned char*>(img.data);

			PacketAerialImage *packet = new PacketAerialImage((unsigned int)std::time(0), 0, 0.f, 0.f, 369, 0.f, 0.f, 0.f, img.rows * img.cols * img.channels(), 0.f, 0.f, 0.f, 0.f, img.rows, img.cols, data);

			Imaging->notifyImageReady(packet);

			img.release();
			delete packet;

			LastSimulation = cur + 1; // Every 1 second
		}
	}

	while (ProcessedPackets.size())
	{
		NetPacket *packet = ProcessedPackets.pop();

		// Here we pull all of the available sessions
		NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

		// And iterate through all of the sessions
		for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
		{
			// Send to all WebSocket sessions
			if (itr->second->getSocketType() == WebSocket && itr->second->getSessionFlag(RequiresCameraImages) == true)
				Network->sendtoSocket(itr->second->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());
		}

		// Insert processed data into databases
		switch (packet->GetPacketHeader())
		{
			case pktAerialImage:
			{
				PacketAerialImage *aerial = reinterpret_cast<PacketAerialImage*>(packet);

				Databases[Aero]->Execute("REPLACE INTO `aero_datacenter`.`aerialimage` VALUES (%d, %d, %f, %f, %f, %f, %f, %f, '%s')", aerial->imageID, aerial->timestamp, aerial->latitude, aerial->longitude,
					aerial->rollAngle, aerial->pitchAngle, aerial->heading, aerial->attitude, aerial->data);

				delete aerial;
			}
			break;

			case pktImageTarget:
				PacketImageTarget *target = reinterpret_cast<PacketImageTarget*>(packet);
    
                std::cout << "Target found, logging to DB: id " << target->targetID << " json: " << target->json << " lat: " << target->latitude << " lon: "<< target->longitude << std::endl;
                /*
                +-----------------+------------+------+-----+---------+-------+
                | Field           | Type       | Null | Key | Default | Extra |
                +-----------------+------------+------+-----+---------+-------+
				| ImageID         | int(11)    | NO   |     | NULL    |       |
                | TargetID        | int(11)    | NO   | PRI | NULL    |       |
                | CvDetected      | tinyint(1) | YES  |     | NULL    |       |
                | Verified        | tinyint(1) | YES  |     | NULL    |       |
                | Latitude        | double     | YES  |     | NULL    |       |
                | Longitude       | double     | YES  |     | NULL    |       |
                +-----------------+------------+------+-----+---------+-------+
                */

				std::stringstream buffer;
				buffer << "REPLACE INTO `aero_datacenter`.`imagetarget` (ImageID, TargetID, DateTime, Description, CvDetected, Verified, Latitude, Longitude) VALUES ("
					<< target->imageID << ", " << target->targetID << ", " << std::time(NULL) << ", '" << target->json << "', " << 1 << ", " << 0 << ", " << target->latitude << ", " << target->longitude << ")";

				Databases[Aero]->Execute(buffer.str().c_str());

				// No idea why this way is bugged.. but I have a workaround at least
				//Databases[Aero]->Execute("REPLACE INTO `aero_datacenter`.`imagetarget` (ImageID, TargetID, DateTime, Description, CvDetected, Verified, Latitude, Longitude)"
				//	"VALUES (%d, %d, %d, '%s', %d, %d, %f, %f)", target->imageID, target->targetID,
				//	std::time(NULL), std::string(target->json).c_str(), 1, 0, target->latitude, target->longitude);

				Databases[Aero]->Execute("REPLACE INTO `aero_datacenter`.`target` VALUES (%d, %d, %d)", target->targetID, target->latitude, target->longitude);
				delete target;
				break;
		}
	}
}
