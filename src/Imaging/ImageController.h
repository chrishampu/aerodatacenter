#pragma once

#include <Network/Packets/PacketImageTransmit.h>
#include <Network/Packets/PacketAerialImage.h>
#include <Common/WorkerQueue.h>
#include <boost/signals2.hpp>
#include <thread>
#include <atomic>

class ImageController
{
public:
	ImageController(bool simulate = false);
	~ImageController();

	void notifyImageReady(NetPacket *packet);

	void ProcessingLoop();
	void Update();

	void StartSimulating() { SimulateLoop = true; }
	void StopSimulating() { SimulateLoop = false; }

private:
	std::atomic_uint ImageCount;
	std::atomic_uint TargetCount;
	boost::signals2::connection image_connection;
	std::thread ImageProcessingThread;
	WorkerQueue<PacketAerialImage*> ProcessingQueue;
	WorkerQueue<NetPacket*> ProcessedPackets;

	bool SimulateLoop;
	unsigned int LastSimulation;

	std::atomic_bool ProcessThread;

	struct ObjectRecValues
	{
		ObjectRecValues()
		{
			Gaussian = 21;
			CannyLow = 78;
			CannyHigh = 137;
			HoughVote = 30;
			HoughLength = 0;
			HoughDistance = 0;
			ImageScale = 50 / 100.0f;
			PolyDPError = 30 / 1000.0f;
		}

		std::atomic_uchar Gaussian;
		std::atomic_uchar CannyLow;
		std::atomic_uchar CannyHigh;
		std::atomic_uchar HoughVote;
		std::atomic_uchar HoughLength;
		std::atomic_uchar HoughDistance;
		double ImageScale;
		double PolyDPError;
	} ObjectRecognitionValues;

public:
	typedef ObjectRecValues ObjectRecValuesT;
	ObjectRecValuesT &getRecValues() { return ObjectRecognitionValues; }
};

extern ImageController *Imaging;