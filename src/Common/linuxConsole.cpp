#include "linuxConsole.h"
#include <Common/Platform.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>

LinuxConsole *Console = NULL;

LinuxConsole::LinuxConsole()
	: Printer(NULL)
{
	consoleEnabled = false;
}

LinuxConsole::~LinuxConsole()
{

}

void LinuxConsole::printf(TextColor colour, const char *out, ...)
{
	if (isEnabled() == false)
		return;

	// Get the line into a buffer.
	static char buffer[4096];
	int i, outpos;
	va_list args;

	va_start(args, out);

	vsprintf(buffer, out, args);

	// Replace tabs with carats, like the real console does.
	char *pos = buffer;
	while (*pos)
	{
		if (*pos == '\t')
			*pos = '^';
		pos++;
	}

	write(STDOUT_FILENO, buffer, strlen(buffer));

	if(Printer->isEnabled() == true)
		Printer->print(buffer);
}

void LinuxConsole::printf(const char *out, ...)
{
	if (isEnabled() == false)
		return;

	// Get the line into a buffer.
	static char buffer[4096];
	int i, outpos;
	va_list args;

	va_start(args, out);

	vsprintf(buffer, out, args);

	// Replace tabs with carats, like the real console does.
	char *pos = buffer;
	while (*pos)
	{
		if (*pos == '\t')
			*pos = '^';
		pos++;
	}

	write(STDOUT_FILENO, buffer, strlen(buffer));

	if(Printer->isEnabled() == true)
		Printer->print(buffer);
}

bool LinuxConsole::isEnabled()
{
	return Console == NULL ? false : Console->consoleEnabled;
}

void LinuxConsole::Create()
{
	if (Console == NULL)
		Console = new LinuxConsole();

	if(Printer == NULL)
		Printer = new ThermalPrinter();
}

void LinuxConsole::Destroy()
{
	if (Console != NULL)
		SAFE_DELETE(Console);

	if(Printer != NULL)
		SAFE_DELETE(Printer);
}

void LinuxConsole::Enable(bool Enabled, const char *Title = "Console") // Either enables or disables the console
{
	consoleEnabled = Enabled;
	if (consoleEnabled == true)
	{
		;
	}
	else
		Destroy();
}

bool LinuxConsole::Process()
{
	if (consoleEnabled == true)
	{
		;
	}

	return true;
}
