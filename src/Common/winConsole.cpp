#include "winConsole.h"

// This is windows-specific functionality
#ifdef PLATFORM_WIN

#include <string>

// Stupid unsafe functions..
#pragma warning(disable: 4996)

winConsole *Console = NULL; // Console will hold the winConsole pointer
ConsoleFunction *ConsoleFunction::first = NULL;

/**
	Here we handle the console exit event sent when a user clicks on the X of
	the console window. We want to fake a clean exit by giving the rest of
	the program time to clean up. The final result is still ungraceful though
	since Windows abruptly kills the program.
*/
int ConsoleEventHandler( DWORD fdwCtrlType ) 
{ 
	switch( fdwCtrlType ) 
	{ 
	// CTRL-CLOSE: confirm that the user wants to exit. 
	case CTRL_CLOSE_EVENT:
		Console->printf(Debug, "Console ungracefully shutting down\r\n");
		Sleep(200);
		return( TRUE ); 
	}

	return(FALSE);
} 

/**
	Basic constructor which by default disables the console until it is created.
*/
winConsole::winConsole()
{
	consoleEnabled = false;
	inputPos = 0;
	memset(inputBuffer, 0, sizeof(inputBuffer));
}

/**
	Console destructor that doesn't yet do anything as the Destroy() function handle deletion
*/
winConsole::~winConsole()
{
}

/**
	Returns whether or not the console is enabled
*/
bool winConsole::isEnabled()
{
	return Console == NULL ? false : Console->consoleEnabled;
}

void winConsole::_printf(const char *out, ...)
{
	DWORD bytes;

	WriteFile(stdOut, out, strlen(out), &bytes, NULL);
	FlushFileBuffers( stdOut );
}

void winConsole::_printf(TextColor color, const char *out, ...)
{
	DWORD bytes;

	// Append a newline if it doesn't exist; basically a total hack
	//if(buffer[strlen(buffer)-1] != '\n')
	//	strcat_s(pos, nSize, "\n");

	// Color the text accordingly
	SetConsoleTextAttribute(stdOut, color);

	// Print it.
	WriteFile(stdOut, out, strlen(out), &bytes, NULL);
	FlushFileBuffers( stdOut );

	// Return text to original color
	SetConsoleTextAttribute(stdOut, Original);
}

/**
	Handles input for the console screen with va args support
*/
void winConsole::printf(TextColor color, const char *out, ...)
{
	if(isEnabled() == false)
		return;

	// Get the line into a buffer.
#ifdef _MSC_VER
	static int BufSize = 4096;
	int nSize;
#endif
	static char buffer[4096];
	char outbuf[512];
	int i, outpos;
	va_list args;

	va_start(args, out);

#ifdef _MSC_VER
	nSize = vsnprintf( buffer, sizeof(BufSize), _TRUNCATE, out, args);
	vsnprintf(buffer, nSize, out, args);
#else
	vsprintf(buffer, out, args);
#endif

	// Replace tabs with carats, like the real console does.
	char *pos = buffer;
	while (*pos)
	{
		if (*pos == '\t')
			*pos = '^';
		pos++;
	}
	
	// Destroy any current input plus prompt character
	memset(outbuf, 0, sizeof(outbuf));
	for(i = outpos = 0; i < inputPos + 7; i++)
	{
		outbuf[outpos ++] = '\b';
		outbuf[outpos ++] = ' ';
		outbuf[outpos ++] = '\b';
	}
	_printf(outbuf);

	// Actual output
	_printf(color, buffer);

	// Return our input and prompt character
	_printf("Aero > ");
	_printf(inputBuffer);
}

void winConsole::printf(const char *out, ...)
{
	if(isEnabled() == false)
		return;

	// Get the line into a buffer.
#ifdef _MSC_VER
	static int BufSize = 4096;
	int nSize;
#endif
	static char buffer[4096];
	char outbuf[512];
	int i, outpos;
	va_list args;

	va_start(args, out);

#ifdef _MSC_VER
	nSize = vsnprintf( buffer, sizeof(BufSize), _TRUNCATE, out, args);
	vsnprintf(buffer, nSize, out, args);
#else
	vsprintf(buffer, out, args);
#endif

	// Replace tabs with carats, like the real console does.
	char *pos = buffer;
	while (*pos)
	{
		if (*pos == '\t')
			*pos = '^';
		pos++;
	}
	
	// Destroy any current input plus prompt character
	memset(outbuf, 0, sizeof(outbuf));
	for(i = outpos = 0; i < inputPos + 7; i++)
	{
		outbuf[outpos ++] = '\b';
		outbuf[outpos ++] = ' ';
		outbuf[outpos ++] = '\b';
	}
	_printf(outbuf);

	// Actual output
	_printf(buffer);

	// Return our input and prompt character
	_printf("Aero > ");
	_printf(inputBuffer);
}

/**
	Creates the console pointer but does not yet enable it
*/
void winConsole::Create()
{
	if(Console == NULL)
		Console = new winConsole();
}

/**
	Safely detach and delete the console
*/
void winConsole::Destroy()
{
	if(Console != NULL)
	{
		FreeConsole(); // Detach current Console and delete it
		SAFE_DELETE(Console); // Safely delete the Console pointer
	}
}

/**
	This either enables or disables the console. If the console is enabled,
	the console is allocated and handles for the I/O are created.
*/
void winConsole::Enable(bool Enabled, const char *Title)
{
	consoleEnabled = Enabled;
	if(consoleEnabled == true)
	{
		CONSOLE_SCREEN_BUFFER_INFO coninfo; // This will hold console information
		DWORD cMode;

		AllocConsole(); // Allocate a console for this process
		SetConsoleTitle(Title); // Assign a window title

		// Assign new handles for I/O
		stdOut = GetStdHandle(STD_OUTPUT_HANDLE); 
		stdIn  = GetStdHandle(STD_INPUT_HANDLE);
		stdErr = GetStdHandle(STD_ERROR_HANDLE);

		// Set screen size to 500 lines so we can scroll text
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
		coninfo.dwSize.Y = MAX_CONSOLE_SIZE;
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

		// Turn off special input handling such as Ctrl-C
		GetConsoleMode(stdIn, &cMode);
		cMode &= ~ENABLE_PROCESSED_INPUT;
		SetConsoleMode(stdIn, cMode);

		_printf("Aero > ");
	}
	else
		Destroy();
}

bool winConsole::Process()
{
	if(consoleEnabled == true)
	{
		DWORD numEvents;
		GetNumberOfConsoleInputEvents(stdIn, &numEvents);
		if(numEvents)
		{
			INPUT_RECORD input[20];
			char outputBuffer[512];
			unsigned int i, outputPos = 0;

			ReadConsoleInput(stdIn, input, 20, &numEvents);

			for(i = 0; i < numEvents; i++)
			{
				if(input[i].EventType = KEY_EVENT)
				{
					KEY_EVENT_RECORD *ke = &(input[i].Event.KeyEvent);
					if(ke->bKeyDown)
					{
						// If escape is pressed then exit
//						if(ke->wVirtualKeyCode == VK_ESCAPE)
//							EventInterface->setRunning(false);

						switch (ke->uChar.AsciiChar)
						{
							case 0:
								switch (ke->wVirtualKeyCode) // For future VK handling
								{
									case VK_UP: // Up arrow key
										break;
								}
								break;
							case '\b': // Backspace
								if(inputPos > 0)
								{
								   outputBuffer[outputPos++] = '\b';
								   outputBuffer[outputPos++] = ' ';
								   outputBuffer[outputPos++] = '\b';
								   inputBuffer[--inputPos] = 0;
								}
								break;
							case '\n': // Newlines
							case '\r':
								{
									// Save our buffer before erasing the main one
									char input[MAX_CONSOLE_SIZE];
									memcpy(input, inputBuffer, sizeof(input));

									// Start with fresh buffers
									memset(outputBuffer, 0, sizeof(outputBuffer));
									memset(inputBuffer, 0, sizeof(inputBuffer));

									// Print the windows newline
									outputBuffer[outputPos++] = '\r';
									outputBuffer[outputPos++] = '\n';
									_printf(outputBuffer);

									// Reset position counters
									inputPos = outputPos = 0;

									// Print the prompt character
									_printf("Aero > ");

									// Process our input
									processConsoleEvent(input);
								}
								break;
							default:
								inputBuffer[inputPos++] = ke->uChar.AsciiChar;
								outputBuffer[outputPos++] = ke->uChar.AsciiChar;
								break;
						}
					}
				}
			}
			if(outputPos)
			{
				outputBuffer[outputPos] = 0;
				_printf(outputBuffer);
			}
		}
	}

	return true;
}

void winConsole::processConsoleEvent(const char *payload)
{
	char func[50];
	char functionPayload[500];

	memset(func, '\0', sizeof(func));
	memset(functionPayload, '\0', sizeof(functionPayload));

	sscanf(payload, "%50s %[^\n]", &func, &functionPayload);

	ConsoleFunction::executeFunction(func, functionPayload);
}

// Console constructor for console functions
ConsoleFunction::ConsoleFunction(char *className, Callback func, int minArgs, int maxArgs)
{
	init(className, minArgs, maxArgs);
	cb = (*func);
}

// Initialize the console function, and attach it to the linked list
void ConsoleFunction::init(const char *cName, int minArgs, int maxArgs)
{
	minA = minArgs;
	maxA = maxArgs;
	className = cName;
	next = first;
	first = this;
}

// Execute a console function given a function name and payload
void ConsoleFunction::executeFunction(const char *cName, const char *payload)
{
	ConsoleFunction *func = findFunction(cName);

	if(func == NULL)
	{
		Console->printf(Debug, "Unknown function: %s\r\n", cName);
		return;
	}

	func->cb(cName, payload, func->minA, func->maxA);
}

// Get a pointer to a console function given its name
ConsoleFunction *ConsoleFunction::findFunction(const char *cName)
{
	for(ConsoleFunction *needle = first; needle; needle = needle->next)
		if(strcmp(needle->className,cName) == 0)
			return needle;

	return NULL;
}

#endif
