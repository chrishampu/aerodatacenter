#pragma once

#include "platformConsole.h"
#include "Platform.h"

#ifdef PLATFORM_WIN

class winConsole : public PlatformConsole
{
public:
	winConsole();
	~winConsole();

	void printf(TextColor color, const char *out, ...);
	void printf(const char *out, ...);

	bool isEnabled();
	void Create();
	void Destroy();
	void Enable(bool Enabled, const char *Title = "Console"); // Either enables or disables the console

	bool Process();

	void processConsoleEvent(const char *payload); // Process a specific ConsoleEvent

protected:
	void _printf(const char *out, ...); // Lower level print function
	void _printf(TextColor color, const char *out, ...);

	HANDLE stdOut; // Handles for the Console I/O
	HANDLE stdIn;
	HANDLE stdErr;

private:
	int inputPos;
	char inputBuffer[MAX_CONSOLE_SIZE];
};

extern winConsole *Console; // Give global access to the console

/** Console Functions
	
	Console funcs are built upon hackeries of hackeries. By using a static
	constructor, they can be defined upon startup before anything else runs,
	and from there can initialize themselved. By using a function pointer
	defined by Callback, and a static function (and definition) created by
	AddConsoleFunction, we can call that function using the Callback.
	By using this Callback typedef, we can also send in additional
	arguments such as our payload.

	Console funcs are held in a linked list fashion for iteration in
	order to find specific console functions.
*/

typedef void (*Callback)(const char *func, const char *payload, int minc, int maxc);

class ConsoleFunction
{
public:
	ConsoleFunction(char *className, Callback func, int minArgs, int maxArgs);

	void init(const char *cName, int minArgs, int maxArgs);

	static void setup();

	static void executeFunction(const char *cName, const char *payload);

private:
	int minA, maxA;
	const char *className;
	Callback cb;

	ConsoleFunction *next;
	static ConsoleFunction *first;

	static ConsoleFunction *findFunction(const char *cName);
};

// Further hackeries. Define a new static function to be used in the Callback,
// and define a ConsoleFunction constructor to initialize the function and callback
#define AddConsoleFunction(name, minArgs, maxArgs)									\
	static void c##name(const char *func, const char *payload, int minc, int maxc);	\
	static ConsoleFunction g##name(#name,c##name, minArgs, maxArgs);					\
	static void c##name(const char *func, const char *payload, int minc, int maxc)

/**
	Console functions are defined by AddConsoleFunction(func name, min args, max args)
	Each function is given its payload string, and can optionally use GetArgs() to
	extract individual information from the payload.
**/

static inline int GetArgs(char **list, const char *haystack, int minargs)
{
	int i = 0;

	char *nextResult;
	char *result = strtok_s(const_cast<char*>(haystack), " ", &nextResult);
	list[i] = result;

	for(i++; i < minargs; i++)
	{
		if((result = strtok_s(NULL, " ", &nextResult)) == NULL)
			break;
		list[i] = result;
	}

	//if(i < minargs)
	//	Console->printf(Debug, "Not enough arguments in console function call\r\n");

	return i; // Return # of arguments
}

// Keep this inline or change to #define? Who knows.
#define hasData(data) (data > NULL)
/**
static inline bool hasData(const char *data)
{
	return (data != NULL);
}
**/

#endif