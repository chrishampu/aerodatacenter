#pragma once

#define MAX_CONSOLE_SIZE 512

// Color codes for the Console text
// The namespace hack is to work around an enum scoping warning
namespace ColorCode
{
	enum TextColor
	{
		Info = 7, // White
		Original = 7,
		MySQL = 8, // Dark Grey
		Networking = 10, // Light Green
		Debug = 11, // Light Blue
		Error = 12, // Light Red
		Assert = 13, // Light Purple
		Flight = 14 // Light Yellow
	};
};

using namespace ColorCode; // Further hackery

class PlatformConsole
{
public:
	PlatformConsole();
	~PlatformConsole();

	virtual void printf(TextColor colour, const char *out, ...) = 0;
	virtual void printf(const char *out, ...) = 0;

	virtual bool isEnabled() = 0;
	virtual void Create() = 0;
	virtual void Destroy() = 0;
	virtual void Enable(bool Enabled, const char *Title = "Console") = 0; // Either enables or disables the console

	virtual bool Process() = 0;

protected:
	bool consoleEnabled; // True/False whether console is enabled
};