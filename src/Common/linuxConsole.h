#ifndef _LINUX_CONSOLE_H_
#define _LINUX_CONSOLE_H_

#include "platformConsole.h"
#include <Logging/ThermalPrinter.h>

class LinuxConsole : PlatformConsole
{
public:
	LinuxConsole();
	~LinuxConsole();


	void printf(TextColor colour, const char *out, ...);
	void printf(const char *out, ...);

	bool isEnabled() ;
	void Create();
	void Destroy();
	void Enable(bool Enabled, const char *Title = "Console"); // Either enables or disables the console

	bool Process();

	ThermalPrinter* getPrinter() { return Printer; }

private:
	bool consoleEnabled; // True/False whether console is enabled
	ThermalPrinter *Printer;
};

extern LinuxConsole *Console; // Give global access to the console

#endif
