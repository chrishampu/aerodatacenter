#pragma once

#include <iostream>

#ifdef _WIN32
	#define PLATFORM_WIN
	#include <windows.h>
	#include "winConsole.h"
#elif __linux
	#define PLATFORM_LINUX
	#include "linuxConsole.h"
#elif __GNUC__
	#define PLATFORM_LINUX
	#include "linuxConsole.h"
#else
	#error No compatible compiler detected
#endif

/**
	We define _DEBUG to use the _Crt* functions for memory debugging.
	Only MSVC supports the _Crt* functions however.
*/
#ifndef _MSC_VER
#	ifdef _DEBUG
#		undef _DEBUG
#	endif
#endif

/**
	If we define new to allocate to Client Blocks, we can get a detailed
	report on which file and line #'s cause memory leaks through memory dumps
	All files that include this file will be forced to use the debug version of new
*/
#ifdef PLATFORM_WIN
	#ifdef _DEBUG
		#define _CRTDBG_MAP_ALLOC
		#include <stdlib.h>
		#include <crtdbg.h>
		#define DEBUG_CLIENTBLOCK new ( _CLIENT_BLOCK , __FILE__ , __LINE__ )
//		#define new DEBUG_CLIENTBLOCK
	#else
		#define DEBUG_CLIENTBLOCK
	#endif
#endif

/* Global function defines */
static inline void safe_delete(void* a)
{
	if( a != nullptr)
	{
		delete a;
		a = nullptr;
	}
}

#undef  SAFE_DELETE
#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL; // Avoiding null pointers

#undef  SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(a) if( (a) != NULL ) delete [] (a); (a) = NULL; // Also avoiding null pointers

#define BYTE2KBYTE(a) (a/1024) // Easy prototype to convery Bytes to KiloBytes

#ifdef _MSC_VER
#	define vsnprintf _vsnprintf_s // Override vsnprintf with the safe version to force safe usage on MSVC
#endif

/* Tool functions */
#define cap_value(a, min, max) ((a >= max) ? max : (a <= min) ? min : a) // Cap value 'a' between min and max

//#define SWAP(a, b) (((a) ^ (b)) && ((b) ^= (a) ^= (b), (a) ^= (b)))
//#define MIN(x, y) (y ^ ((x ^ y) & -(x < y)))
//#define MAX(x, y) (x ^ ((x ^ y) & -(x < y)))