#pragma once

#include <Common/Platform.h>
#include "Markdown/document.h"
#include <wkhtmltox/pdf.h>
#include <sstream>

class ReportGenerator;

class ReportPage
{
	friend class ReportGenerator;
public:
	ReportPage(std::string PageName);
	ReportPage() { }
	~ReportPage();

	virtual void LoadPageContent();
	void LoadPageTemplate(std::string path);

	void GenerateHTML();

	std::stringstream& operator<<(std::string in) { MarkdownInput << in; return MarkdownInput; }

private:
	// For HTML
	void InitMarkdown();
	void ReleaseMarkdown();

	std::stringstream MarkdownInput;

	hoedown_document *MarkdownDocument;
	hoedown_renderer *MarkdownRenderer;
	hoedown_buffer *MarkdownInBuffer;
	hoedown_buffer *MarkdownOutBuffer;

	FILE *MarkdownOutputFile;

protected:
	// So generator can access it
	std::string MarkdownOutputName;

	// PDF specific
	wkhtmltopdf_object_settings *PageSettings;
};