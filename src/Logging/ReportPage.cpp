#include "ReportPage.h"
#include "Markdown/html.h"
#include "Markdown/buffer.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

ReportPage::ReportPage(std::string PageName)
	: MarkdownOutputName(PageName)
{

}

ReportPage::~ReportPage()
{

}

void ReportPage::LoadPageContent()
{
	*this << "## <center>This page intentionally left blank.</center> ##\n*****\n";
}

void ReportPage::LoadPageTemplate(std::string path)
{
	std::ifstream file(path, std::ios::in | std::ios::binary | std::ios::ate);

	if (file.is_open())
	{
		unsigned int size = (unsigned int)file.tellg();
		char *data = new char[size];
		memset(data, 0, size);

		file.seekg(0, std::ios::beg);
		file.read(data, size);

		file.close();

		std::string in;
		in.insert(0, data, size);

		*this << in;

		Console->printf(Debug, "Loaded page template: %s\n", path.c_str());

		delete[] data;

		return;
	}

	Console->printf(Error, "Failed to load page template: %s\n", path.c_str());
}

void ReportPage::GenerateHTML()
{
	InitMarkdown();

	hoedown_buffer_put(MarkdownInBuffer, MarkdownInput.str().c_str(), MarkdownInput.str().size());

	hoedown_document_render(MarkdownDocument, MarkdownOutBuffer, MarkdownInBuffer->data, MarkdownInBuffer->size);

	fwrite(MarkdownOutBuffer->data, MarkdownOutBuffer->size, 1, MarkdownOutputFile);

	ReleaseMarkdown();
}


void ReportPage::InitMarkdown()
{
	static int MdInAllocSize = 1024;
	static int MdOutAllocSize = 64;
	static int MdMaxNesting = 16;

	MarkdownInBuffer = hoedown_buffer_new(MdInAllocSize);
	MarkdownOutBuffer = hoedown_buffer_new(MdInAllocSize);

	MarkdownRenderer = hoedown_html_renderer_new(0, 0);
	MarkdownDocument = hoedown_document_new(MarkdownRenderer, 0, MdMaxNesting);

	MarkdownOutputFile = fopen(MarkdownOutputName.c_str(), "w");
}

void ReportPage::ReleaseMarkdown()
{
	hoedown_buffer_free(MarkdownInBuffer);
	hoedown_buffer_free(MarkdownOutBuffer);

	hoedown_html_renderer_free(MarkdownRenderer);
	hoedown_document_free(MarkdownDocument);

	fclose(MarkdownOutputFile);
}
