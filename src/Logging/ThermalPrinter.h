#ifndef _THERMAL_PRINTER_H_
#define _THERMAL_PRINTER_H_

//#include <Common/Platform.h>

class ThermalPrinter
{
public:
	ThermalPrinter();
	~ThermalPrinter();

	void printf(const char *out, ...);
	void print(const char *out);

	bool isEnabled() { return PrinterEnabled; }
private:
	void SetupSerial();

	int serialHandle;
	bool PrinterEnabled;
};

#endif
