#include "ReportGenerator.h"
#include <Network/NetPlatform.h>
#include <Network/NetHTTPInterpreter.h>

#ifdef PLATFORM_WIN
#include <memory>

AddConsoleFunction(gen_report, 0, 0)
{
	ReportGenerator::CreateAeroReport();
}
#endif

void ReportGenerator::CreateAeroReport()
{
	// Just some pointer witchcraft
	std::shared_ptr<ReportGenerator> ptr = std::shared_ptr<ReportGenerator>(new ReportGenerator());
	ReportGenerator &gen = *ptr.get();

	gen.AddPage(new TitlePage("title.html"));
	gen.AddPage(new TargetPage("targets.html"));

	gen.GenerateReport();
}

ReportGenerator::ReportGenerator(std::string reportOut)
	: ReportOutputName(reportOut)
{
	wkhtmltopdf_init(true);
}

ReportGenerator::~ReportGenerator()
{
	for (ReportPage *page : Pages)
		delete page;

//	wkhtmltopdf_deinit(); // This breaks consecutive report generations
}

void ReportGenerator::AddPage(ReportPage *page)
{
	Pages.push_back(page);
}

void ReportGenerator::GeneratePDF()
{
	wkhtmltopdf_convert(PDFConverter);
}

void ReportGenerator::GenerateReport(std::string path)
{
	if (path.length() > 0)
	{
		for (ReportPage *page : Pages)
			page->MarkdownOutputName.insert(0, path);
		ReportOutputName.insert(0, path);
	}

	InitPDF();

	for (ReportPage *page : Pages)
	{
		Console->printf(Debug, "Creating page: %s\n", page->MarkdownOutputName.c_str());

		page->LoadPageContent();
		page->GenerateHTML();

		InitPageSettings(page);

		wkhtmltopdf_add_object(PDFConverter, page->PageSettings, "");
	}

	Console->printf(Debug, "Report being written to: %s\n", ReportOutputName.c_str());

	GeneratePDF();

	ReleasePDF();

	PacketTransmitReport *report = new PacketTransmitReport(ReportOutputName.c_str(), ReportOutputName.length());

	NetHTTPInterpreter::EncodeAsFrame(report, BinaryFrame);

	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
		if (itr->second->getSessionState().getClientType() == SystemAnalyst)
			Network->sendtoSocket(itr->second->getSocket(), report->PacketData.getBuffer(), report->getPacketSize());
}

void ReportGenerator::InitPDF()
{
	PDFGlobalSettings = wkhtmltopdf_create_global_settings();
	PDFObjectSettings = wkhtmltopdf_create_object_settings();

	InitPDFSettings();

	PDFConverter = wkhtmltopdf_create_converter(PDFGlobalSettings);

	wkhtmltopdf_set_warning_callback(PDFConverter, &ReportGenerator::CallbackPDFWarning);
	wkhtmltopdf_set_error_callback(PDFConverter, &ReportGenerator::CallbackPDFError);
	wkhtmltopdf_set_phase_changed_callback(PDFConverter, &ReportGenerator::CallbackPDFPhaseChanged);
	wkhtmltopdf_set_progress_changed_callback(PDFConverter, &ReportGenerator::CallbackPDFProgressChanged);
	wkhtmltopdf_set_finished_callback(PDFConverter, &ReportGenerator::CallbackPDFFinished);
}

void ReportGenerator::InitPDFSettings()
{
	std::string out = "web/" + ReportOutputName;

//	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "size.paperSize", "A4");
//	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "size.width", "4cm");
//	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "size.height", "12in");
//	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "size.orientation", "Portrait");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "colorMode", "Color");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "copies", "1");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "collate", "false");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "outline", "false");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "dumpOutline", "");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "out", out.c_str());
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "documentTitle", "Aero Report");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "useCompression", "false");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "margin.top", "2cm");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "margin.bottom", "2cm");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "margin.left", "1cm");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "margin.right", "1cm");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "outputFormat", "pdf");
//	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "imageDPI", "80");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "imageQuality", "92");
	wkhtmltopdf_set_global_setting(PDFGlobalSettings, "load.CookieJar", "");
}

void ReportGenerator::InitPageSettings(ReportPage *page)
{
	page->PageSettings = wkhtmltopdf_create_object_settings();

	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.useDottedLines", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.captionText", "");
	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.forwardLinks", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.backLinks", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.indentation", "2em");
	wkhtmltopdf_set_object_setting(page->PageSettings, "toc.fontScale", "0.8");
	wkhtmltopdf_set_object_setting(page->PageSettings, "page", page->MarkdownOutputName.c_str());
	wkhtmltopdf_set_object_setting(page->PageSettings, "useExternalLinks", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "useLocalLinks", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "produceForms", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.background", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.loadImages", "true");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.enableJavascript", "true");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.enableIntelligentShrinking", "true");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.minimumFontSize", "9");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.printMediaType", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.defaultEncoding", "utf-8");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.userStyleSheet", "");
	wkhtmltopdf_set_object_setting(page->PageSettings, "web.enablePlugins", "false");
	wkhtmltopdf_set_object_setting(page->PageSettings, "includeInOutline", "false");
}

void ReportGenerator::ReleasePDF()
{
//	wkhtmltopdf_destroy_global_settings(PDFGlobalSettings);

	wkhtmltopdf_destroy_converter(PDFConverter);
}

void ReportGenerator::CallbackPDFWarning(wkhtmltopdf_converter * converter, const char *str)
{
	Console->printf(Debug, "PDF warning: %s\n", str);
}

void ReportGenerator::CallbackPDFError(wkhtmltopdf_converter * converter, const char *str)
{
	Console->printf(Error, "PDF error: %s\n", str);
}

void ReportGenerator::CallbackPDFPhaseChanged(wkhtmltopdf_converter * converter)
{
	Console->printf(Debug, "Report state: %s\n", wkhtmltopdf_phase_description(converter, wkhtmltopdf_current_phase(converter)));
}

void ReportGenerator::CallbackPDFProgressChanged(wkhtmltopdf_converter * converter, int val)
{
	Console->printf(Debug, "Report status: %d%s\n", val, "%");
}

void ReportGenerator::CallbackPDFFinished(wkhtmltopdf_converter * converter, int val)
{

}
