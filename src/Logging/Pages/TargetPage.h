#pragma once

#include "../ReportPage.h"

class TargetPage : public ReportPage
{
public:
	TargetPage(std::string PageName) : ReportPage(PageName) {}
	~TargetPage() {}

	void LoadPageContent();
};