#pragma once

#include "../ReportPage.h"

class TitlePage : public ReportPage
{
public:
	TitlePage(std::string PageName) : ReportPage(PageName) {}
	~TitlePage() {}

	void LoadPageContent();
};