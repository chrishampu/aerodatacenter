#include "TargetPage.h"
#include <Database/DatabaseManager.h>

void TargetPage::LoadPageContent()
{
	// HTML code can be loaded through a file
	LoadPageTemplate("report/target_page_header.html");

	std::shared_ptr<QueryResult> ptr = Databases[Aero]->Query("SELECT * FROM aero_datacenter.imagetarget");
	// Ensure we actually have data returned
	if (ptr != nullptr)
	{
		QueryResult *res = ptr.get();

		// Or iterated and dumped from a database
		while (res->NextRow())
		{
			Field* fields = res->Fetch();

			int image = fields[0].GetInt();
			int timestamp = fields[2].GetInt();
			float lat = fields[6].GetFloat();
			float lon = fields[7].GetFloat();

			*this << "<p> Image " << image
				<< "<div>Timestamp: " << timestamp << "</div>"
				<< "<span>Lat: " << lat << "  Lon: " << lon << "</span>"
				<< "</p>";
		}
	}
	else
	{
		Console->printf(Debug, "No targets detected to report\n");
		*this << "<p>No targets detected</p>";
	}

	// And then topped off with more templates
	LoadPageTemplate("report/target_page_footer.html");
}
