#include "ThermalPrinter.h"
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "fcntl.h"

ThermalPrinter::ThermalPrinter()
{
	PrinterEnabled = false;
	SetupSerial();
}

ThermalPrinter::~ThermalPrinter()
{

}

void ThermalPrinter::printf(const char *out, ...)
{
	if(PrinterEnabled == false)
		return;

	puts("Unsupported function");
}

void ThermalPrinter::print(const char *out)
{
	if(PrinterEnabled == false)
		return;

	write(serialHandle, out, strlen(out));
}

void ThermalPrinter::SetupSerial()
{
	serialHandle = open("/dev/ttyUSB0",O_RDWR|O_NOCTTY|O_SYNC);
 
	if(serialHandle == -1)
	{
		puts("Failed to open serial to Thermal Printer. Disabled");
	}
	else
	{
		struct termios options;
		
		fcntl(serialHandle, F_SETFL, FNDELAY);
	
		tcgetattr(serialHandle, &options); /* Get the current options for the port */
		
		cfsetispeed(&options, B19200); /* Set the baud rates to 19200/ */
		cfsetospeed(&options, B19200);
		options.c_cflag |= (CLOCAL); /* Enable the receiver and set local mode */
		options.c_cflag &= ~PARENB; /* Mask the character size to 8 bits, no parity */
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8; /* Select 8 data bits */
		options.c_cflag &= ~CRTSCTS; /* Disable hardware flow control */
		options.c_lflag &= ~(ICANON | ECHO | ISIG);/* Enable data to be processed as raw input */

		options.c_cc[VMIN] = 0;
		options.c_cc[VTIME] = 0;		
		
		tcsetattr(serialHandle, TCSANOW, &options); /* Set the new options for the port */

		PrinterEnabled = true;

		write(serialHandle, "Thermal printer enabled\n", 25);
	}
}
