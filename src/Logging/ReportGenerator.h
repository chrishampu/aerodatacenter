#pragma once

#include "ReportPage.h"
#include "Pages/TitlePage.h"
#include "Pages/TargetPage.h"
#include <Common/Platform.h>
#include <wkhtmltox/pdf.h>
#include <vector>

class ReportGenerator
{
public:
	ReportGenerator(std::string reportOut = "report.pdf");
	~ReportGenerator();

	void AddPage(ReportPage *page);

	void GeneratePDF();
	void GenerateReport(std::string path = "");

	static void CreateAeroReport();

private:
	void InitPDF();
	void InitPDFSettings();
	void InitPageSettings(ReportPage *page);

	void ReleasePDF();

	static void CallbackPDFWarning(wkhtmltopdf_converter * converter, const char *str);
	static void CallbackPDFError(wkhtmltopdf_converter * converter, const char *str);
	static void CallbackPDFPhaseChanged(wkhtmltopdf_converter * converter);
	static void CallbackPDFProgressChanged(wkhtmltopdf_converter * converter, int val);
	static void CallbackPDFFinished(wkhtmltopdf_converter * converter, int val);

	std::string ReportOutputName;

	wkhtmltopdf_global_settings *PDFGlobalSettings;
	wkhtmltopdf_object_settings *PDFObjectSettings;
	wkhtmltopdf_converter *PDFConverter;

	std::vector<ReportPage*> Pages;
};