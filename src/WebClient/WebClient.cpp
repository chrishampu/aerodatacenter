#include "WebClient.h"
#include <Network/NetPacketManager.h>
#include <Network/NetHTTPInterpreter.h>
#include <Network/NetPlatform.h>
#include <Database/DatabaseManager.h>
#include <Logging/ReportGenerator.h>
#include <ctime>

WebClient::WebClient()
{
	client_connection = PacketManager->connectPacketReceiver(boost::bind(&WebClient::PacketReceived, this, _1));
}

WebClient::~WebClient()
{
	client_connection.disconnect();
}

void WebClient::PacketReceived(NetPacket *packet)
{
	if(packet->GetPacketHeader() != pktClientMessage)
		return;

//  We need to cast the generic NetPacket to the specific structure we want
	PacketClientMessage *clientpacket = reinterpret_cast<PacketClientMessage*>(packet);

	TryParseCommand(reinterpret_cast<const char*>(clientpacket->getMsg()));
}

void WebClient::TryParseCommand(std::string command)
{
	PacketSystemLog *reply = nullptr;
	PacketClientCommand *relay = nullptr;
	std::string replyMessage = "Successfully parsed command";
	int level = 1;
	int commandTarget = 0;

	if (command.find(':') == std::string::npos)
	{
		replyMessage = "Poorly crafted command";
		level = 3;
	}
	else
	{
		std::string target = command.substr(0, command.find(':'));
		std::string msg = command.substr(command.find(':') + 1, command.size());

		if (target.compare("AT") == 0)
			commandTarget = 1;
		else if (target.compare("UAV") == 0)
			commandTarget = 2;

		if (commandTarget != 0)
		{
			relay = new PacketClientCommand((unsigned int)std::time(NULL), commandTarget, 0, msg.size(), msg.c_str());
			Console->printf(Debug, "ClientCommandMessage: Relayed to %s system\n", commandTarget == PacketClientCommand::SystemAT ? "AT" : "UAV");
		}
		else
			replyMessage = ExecuteCommand(msg);
	}

	reply = new PacketSystemLog((unsigned int)std::time(NULL), level, PacketSystemLog::DC, replyMessage.size(), replyMessage.c_str());

	NetHTTPInterpreter::EncodeAsFrame(reply, BinaryFrame);

	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
	{
		if (itr->second->getSessionState().getClientType() == SystemAnalyst)
			Network->sendtoSocket(itr->second->getSocket(), reply->PacketData.getBuffer(), reply->getPacketSize());
		else if (itr->second->getSessionState().getClientType() == commandTarget && commandTarget != 0)
			Network->sendtoSocket(itr->second->getSocket(), relay->PacketData.getBuffer(), relay->getPacketSize());
	}

	if (relay != nullptr && reply != nullptr)
		Databases[Aero]->Execute("INSERT INTO `systemcommand` (Command, Parameters, DateTime, Accepted, SenderID, RecipientID) VALUES ('%s', '%s', %d, %d, %d, %d)",
			relay->getMsg(), "", relay->getTime(), 1, reply->getLevel(), reply->getSystem());

	delete reply;
	if (relay != nullptr)
		delete relay;
}

std::string WebClient::ExecuteCommand(std::string command)
{
	if (strcmp(command.c_str(), "report") == 0)
	{
		ReportGenerator::CreateAeroReport();

		return "Report written to disk";
	}

	return "Invalid command";
}