#pragma once

#include <boost/signals2.hpp>
#include <thread>

class NetPacket;

class WebClient
{
public:
	WebClient();
	~WebClient();

	void PacketReceived(NetPacket *packet);

	void Main();

	void Update();

	void TryParseCommand(std::string command);
	std::string ExecuteCommand(std::string command);

private:
	std::thread WebClientThread;
	boost::signals2::connection client_connection;
};
