#include <Common/Platform.h>
#include <Common/Base64.h>
#include <Network/NetPlatform.h>
#include <Network/NetPacketManager.h>
#include <Network/NetHTTPInterpreter.h>
#include <Imaging/ImageController.h>
#include <WebClient/WebClient.h>
#include <WebClient/Navigation.h>
#include <Database/DatabaseManager.h>
#include <Logging/ReportGenerator.h>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK

bool Running = true;

AddConsoleFunction(exit, 0, 0)
{
	Running = false;
}

AddConsoleFunction(sendimage, 1, 1)
{
	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	// We need at least 1 session to send to
	if (sessions.size() == 0)
		return;

	// Retrieve the first session, so we can "fake" the packet coming from it
	NetSessionManager::SessionMap::iterator first = sessions.begin();
	
	cv::Mat img = cv::imread("targets.JPG");

	const char *data = reinterpret_cast<const char*>(img.data);

	PacketImageTransmit *pack = new PacketImageTransmit(img.rows, img.cols, data, img.rows * img.cols * img.channels());

	Imaging->notifyImageReady(pack);

	img.release();
	delete pack;
}
#endif

void print_help()
{
	std::stringstream help;

	help << "AERO Datacenter\n\n"
		<< "Options:\n"
		<< "-h --help: Displays this info\n"
		<< "--simulate: Simulate image processing every second\n";

	Console->printf(Debug, "%s", help.str().c_str());
}

int main(int argc, char **argv)
{
#ifdef PLATFORM_LINUX
	Console = new LinuxConsole();
#else
	Console = new winConsole();
#endif
	Console->Create();
	Console->Enable(true);

#ifdef PLATFORM_WIN
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
#endif
#endif

	bool simulate = false;

	// Process command line
	for (int i = 1; i < argc; i++)
	{
		char *arg = argv[i];
		bool parsed = false;

		if (!arg[0])
		{
			Console->printf(Debug, "Invalid command line option: %s\n", arg);
			return 1;
		}

		// Short commands are -
		// Long commands are  --
		if (arg[1] != '-')
		{
			char opt = arg[1];

			if (opt == 'h') 
			{
				print_help();
				Console->Destroy();
				return 1;
			}

			if (parsed == false)
			{
				Console->printf(Debug, "Invalid command line option: %c\n", opt);
				return 1;
			}
		}

		if (!arg[2])
		{
			Console->printf(Debug, "Invalid command line option: %s\n", arg);
			return 1;
		}

		char opt[100];
		strncpy(opt, arg + 2, 100);
		opt[99] = 0;

		char *val = strchr(opt, '-');
		
		if (strcmp(opt, "help") == 0)
		{
			print_help();
			Console->Destroy();
			return 1;
		}

		if (strcmp(opt, "simulate") == 0)
		{
			simulate = true;
			parsed = true;
			Console->printf(Debug, "Simulating image processing\n", opt);
		}

		if (parsed == false)
		{
			Console->printf(Debug, "Invalid command line option: %s\n", opt);
			return 1;
		}
	}

	Network = new NetPlatform();
	Network->Init();

	Databases.AddDatabase(Aero, new Database("localhost", "aero", "aerob150", "aero_datacenter", 3306));

	// Clean tables
	Databases[Aero]->Execute("DELETE FROM `aerialimage`");
	Databases[Aero]->Execute("DELETE FROM `imagetarget`");
	Databases[Aero]->Execute("DELETE FROM `target`");

	Imaging = new ImageController(simulate);
	WebClient *client = new WebClient();
	NavigationManager = new Navigation();

	int server = Network->openListenPort("192.168.72.10", 24000);

#ifdef PLATFORM_WIN
	while (Running)
	{
		Console->Process();
#else
	while (true)
	{
#endif
		Network->Process();

		Imaging->Update();
	}

	Network->Shutdown();
	Databases.Shutdown();

	SAFE_DELETE(NavigationManager);
	SAFE_DELETE(Imaging);
	SAFE_DELETE(client);
	SAFE_DELETE(Network);

	Console->Destroy();

	return 0;
}
