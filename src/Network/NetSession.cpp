#include "NetPlatform.h"
#include "NetSession.h"
#include "NetPacketManager.h"
#include "NetHTTPInterpreter.h"
#include "Packets/PacketRecognitionValues.h"
#include <Imaging/ImageController.h>
#include <string>

NetSession::NetSession(NetSocket sock, NetAddress addr)
	: Socket(sock),
	  Address(addr),
      SocketType(Undetermined),
	  HandshakeComplete(false),
	  ConnectionState(InvalidState)
{
	SessionState.SessionFlags.reset();
	SessionState.ClientType = ClientUnknown;
	SessionState.parent = this;
}

NetSession::~NetSession()
{
	while(!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		delete packet;

		PacketQueue.pop();
	}

	Network->closeSocket(Socket);
}

bool NetSession::Update()
{
	while(!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		if(SocketType == Undetermined)
		{
			if(NetHTTPInterpreter::isHTTPPacket(packet) == true)
			{
				if (NetHTTPInterpreter::HandleGetRequest(packet, this) == true)
				{
					setSocketType(HTTPSocket);
					delete packet;
				}
				else
				{
					setSocketType(WebSocket);

					// Start a thread and detach it to do long handshake processing and ensure main loop isn't blocked
					// The AuthenticateHandshake function will delete the packet data when it no longer requires it
					NetHTTPInterpreter *interpreter = new NetHTTPInterpreter(packet, this);
					interpreter->AuthenticateHandshake();
				}

				PacketQueue.pop();
				continue;
			}
			else
				setSocketType(TCPSocket);
		}

		if(SocketType == WebSocket && HandshakeComplete == true)
		{
			std::unique_ptr<NetHTTPInterpreter> interpreter(new NetHTTPInterpreter(packet, this));
			interpreter->InterpretPacket();
		}
		else if (SocketType == TCPSocket)
			PacketManager->processPacketReceivedEvent(packet, this);
		else if (SocketType == HTTPSocket)
			NetHTTPInterpreter::HandleGetRequest(packet, this);

		delete packet;

		PacketQueue.pop();
	}

	if(SessionState.SessionFlags[RequiresPositionInfo])
	{
		// Here we can send any information this session requires
	}

	return true;
}

void NetSession::AddPacket(NetPacket *packet)
{
	PacketQueue.push(packet);
}

std::string NetSession::getAddressString()
{
	return Network->NetAddressToString(&Address);
}

void NetSession::SessionStateT::setClientType(ClientTypeEnum type)
{
	ClientType = type;

	switch (ClientType)
	{
		default:
		case ClientUnknown:
			Console->printf(Networking, "Unknown client connected\n");
			break;
		case ImageAnalyst:
			{
				Console->printf(Networking, "Image Analyst client connected\n");

				SessionFlags[RequiresCameraImages] = true;

				ImageController::ObjectRecValuesT &values = Imaging->getRecValues();

				PacketRecognitionValues *packet = new PacketRecognitionValues(values.Gaussian, values.CannyLow, values.CannyHigh,
					values.HoughVote, values.HoughLength, values.HoughDistance);

				NetHTTPInterpreter::EncodeAsFrame(packet, BinaryFrame);
				Network->sendtoSocket(parent->Socket, packet->getPacketBuffer(), packet->getPacketSize());
				delete packet;
			}
			break;
		case SystemAnalyst:
			Console->printf(Networking, "System Analyst client connected\n");
			break;
		case Recon:
			break;
		case Spectator:
			break;
		case Judge:
			break;
		case AntennaTracker:
			Console->printf(Networking, "Antenna Tracker client connected\n");
			break;
		case ClientUAV:
			Console->printf(Networking, "UAV client connected\n");
			break;
	}
}
