#include "NetHTTPResponse.h"

HTTPResponse::HTTPResponse()
{
	Code = 0;
	Content = "";
}

HTTPResponse::HTTPResponse(short code)
{
	Code = code;
	Content = "";
}

HTTPResponse::~HTTPResponse()
{

}

void HTTPResponse::SetResponseCode(short code)
{
	Code = code;
}

void HTTPResponse::SetHeader(std::string header, std::string value)
{
	HeaderMap[header] = value;
}

void HTTPResponse::SetContent(std::string content)
{
	Content = content;
}

void HTTPResponse::SetContent(char *data, int size)
{
	Content.clear();
	Content.append(data, size);
}

std::string HTTPResponse::GetResponseBuffer()
{
	std::stringstream buffer;

	buffer << ResponseCodeToString(Code) << "\r\n";

	 for (HeaderMapT::iterator iter = HeaderMap.begin(); iter != HeaderMap.end(); ++iter)
		buffer << iter->first << ": " << iter->second << "\r\n";

	buffer << "\r\n";

	if (Content.size() > 0)
		buffer << Content;

	return buffer.str();
}

// Very, very sub optimal way of handling this
int HTTPResponse::GetResponseSize()
{
	return GetResponseBuffer().length();
}

std::string HTTPResponse::ResponseCodeToString(short code)
{
	switch (code)
	{
	case 101:
		return "HTTP/1.1 101 Switching Protocols";
	case 200:
		return "HTTP/1.1 200 OK";
	case 404:
		return "HTTP/1.1 404 Not Found";
	default:
		return "Unsupported response code";
	}
}