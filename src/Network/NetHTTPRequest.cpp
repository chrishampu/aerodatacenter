#include "NetHTTPRequest.h"
#include <algorithm>

HTTPRequest::HTTPRequest()
{
	Method = "";
	Url = "";
}

// Initialize this request with a pre-existing buffer
HTTPRequest::HTTPRequest(std::string buffer)
{
	Method = "";
	Url = "";

	ConsumeBuffer(buffer);
}

HTTPRequest::~HTTPRequest()
{

}

void HTTPRequest::SetUrl(std::string url)
{
	Url = url;
}

void HTTPRequest::SetRequestMethod(std::string method)
{
	Method = method;
}

void HTTPRequest::SetHeader(std::string header, std::string value)
{
	HeaderMap[header] = value;
}

void HTTPRequest::ConsumeBuffer(std::string buffer)
{
	std::string::iterator begin = buffer.begin();
	std::string::iterator end;

	for (;;)
	{
		// Find a line to process
		end = std::search(
			begin,
			buffer.end(),
			"\r\n",
			"\r\n" + sizeof("\r\n")-1
			);

		// No more data to read
		if (end == buffer.end())
			return;

		// We found a blank line in this case, and the request buffer is complete
		if (end - begin == 0)
		{
			return;
		}
		else
		{
			if (Method.length() == 0)
			{
				// Process method
				std::string::iterator cursor_start = begin;
				std::string::iterator cursor_end = std::find(begin, end, ' ');

				if (cursor_end == end) {
					continue;
				}

				Method = std::string(cursor_start, cursor_end);

				cursor_start = cursor_end + 1;
				cursor_end = std::find(cursor_start, end, ' ');

				if (cursor_end == end) {
					continue;
				}

				Url = std::string(cursor_start, cursor_end);
				std::string version = std::string(cursor_end + 1, end);
			}
			else
			{
				// Process header
				std::string::iterator cursor = std::search(
					begin,
					end,
					":",
					":" + sizeof(":") - 1
					);

				// Invalid header
				if (cursor == end) {
					continue;
				}

				HeaderMap[std::string(begin, cursor)] = std::string(cursor + sizeof(":"), end);
			}
		}

		begin = end + (sizeof("\r\n") - 1);
	}
}

std::string HTTPRequest::GetMethod()
{
	return Method;
}

std::string HTTPRequest::GetHeader(std::string header)
{
	HeaderMapT::iterator iter = HeaderMap.find(header);

	return iter != HeaderMap.end() ? iter->second : "";
}

std::string HTTPRequest::GetUrl()
{
	return Url;
}