#pragma once

#include "Packets/PacketClientMessage.h"
#include "Packets/PacketImageTransmit.h"
#include "Packets/PacketSystemLog.h"
#include "Packets/PacketAerialImage.h"
#include "Packets/PacketRecognitionValues.h"
#include "Packets/PacketClientType.h"
#include "Packets/PacketClientCommand.h"
#include "Packets/PacketImageTarget.h"
#include "Packets/PacketTriggerReport.h"
#include "Packets/PacketTransmitReport.h"
#include "Packets/PacketOnboardImage.h"
#include "Packets/PacketRejectImage.h"