#pragma once

#include "NetSocket.h"
#include "NetPacketList.h"

#include <boost/signals2.hpp>
#include <queue>
#include <bitset>

enum NetSessionType {
	Undetermined = 0,
	WebSocket,
	TCPSocket,
	HTTPSocket
};

enum ConnectionStateType {
	InvalidState = -1,
	ConnectionPending,
	Connected,
	NameLookupRequired,
	Listening,
	Closed
};

enum SessionFlagType {
	RequiresGPSInfo = 0,
	RequiresCameraImages,
	RequiresPositionInfo,
	HasMissionPlanner,
	SessionFlagLast
};

class NetSession
{
public:
	NetSession(NetSocket sock, NetAddress addr);
	~NetSession();

	bool Update();

	void AddPacket(NetPacket *packet);

	ConnectionStateType ConnectionState;

private:
	struct SessionStateT
	{
		friend class NetSession;
	public:
		void setClientType(ClientTypeEnum type);
		ClientTypeEnum getClientType() { return ClientType; }

	private:
		ClientTypeEnum ClientType;

		std::bitset<SessionFlagLast> SessionFlags;

		NetSession *parent;
	} SessionState;

	NetSessionType SocketType;
	NetAddress Address;
	NetSocket Socket;

	typedef std::queue<NetPacket*> PacketQueueT;
	PacketQueueT PacketQueue;

public:
	// WebSocket-related variables
	bool HandshakeComplete;

	// Misc set/get functions
	NetSocket getSocket() { return Socket; }
	NetAddress getAddress() { return Address; }
	NetSessionType getSocketType() { return SocketType; }
	void setSocketType(NetSessionType type) { SocketType = type; }

	SessionStateT &getSessionState() { return SessionState; };
	bool getSessionFlag(SessionFlagType flag) { return SessionState.SessionFlags[flag]; }

	std::string getAddressString();
};
