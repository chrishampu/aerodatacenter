#pragma once

/** This implementation of WebSockets follows RFC6455 (http://tools.ietf.org/html/rfc6455) **/

#include "NetPacket.h"
#include "NetSession.h"
#include <map>

enum WebSocketOpcode
{
	TextFrame = 0x1,
	BinaryFrame,
	CloseFrame = 0x8,
	PingFrame,
	PongFrame
};

enum FrameErrorCode
{
	NormalClosure = 1000,
	GoingAway,
	ProtocolError,
	UnacceptableData,
	Reserved,
	Reserved2,
	Reserved3,
	InconsistentData,
	PolicyViolation,
	OversizedMessage,
	ExtensionNegotiationFail,
	UnexpectedCondition,
};

class NetHTTPInterpreter {
public:
	NetHTTPInterpreter(NetPacket *packet, NetSession *session);
	~NetHTTPInterpreter();

	void InterpretPacket();

	void AuthenticateHandshake();
	std::string CalcAcceptKey(std::string key);
	
	static bool HandleGetRequest(NetPacket *packet, NetSession *session);
	static void SendFileToSession(std::string path, NetSession* session);
	static void EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode);
	static bool isHTTPPacket(NetPacket *packet);
	static std::string ErrorCodeToString(FrameErrorCode code);

private:
	NetPacket *httpPacket;
	NetSession *httpSession;
};

static const char *DocumentRoot = "web";