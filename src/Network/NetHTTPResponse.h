#pragma once

#include <sstream>
#include <map>

class HTTPResponse
{
public:
	HTTPResponse();
	HTTPResponse(short code);
	~HTTPResponse();

	void SetResponseCode(short code);
	void SetHeader(std::string header, std::string value);
	void SetContent(std::string content);
	void SetContent(char *data, int size);

	std::string GetResponseBuffer();
	int GetResponseSize();

	std::string ResponseCodeToString(short code);

private:
	typedef std::map<std::string, std::string> HeaderMapT;
	HeaderMapT HeaderMap;

	short Code;
	std::string Content;
};