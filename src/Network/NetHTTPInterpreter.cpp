#include "NetHTTPInterpreter.h"
#include "NetHTTPResponse.h"
#include "NetHTTPRequest.h"
#include "NetPlatform.h"
#include "NetPacketManager.h"
#include <Common/SHA1.h>
#include <Common/Base64.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <thread>

#ifdef PLATFORM_LINUX
#include <netinet/in.h>
#endif

NetHTTPInterpreter::NetHTTPInterpreter(NetPacket *packet, NetSession *session)
{
	httpPacket = packet;
	httpSession = session;
}

NetHTTPInterpreter::~NetHTTPInterpreter()
{

}

void NetHTTPInterpreter::InterpretPacket()
{
	const unsigned char *data; // This holds static data
	std::stringstream buffer; // This is an intermediary buffer that can easily be written to, unlike a string

	buffer << httpPacket->PacketData.getBuffer();
	std::string stringdata = buffer.str();
	data = reinterpret_cast<const unsigned char*>(stringdata.c_str());

	unsigned int dataStart = 2;
	unsigned int payloadSize = 0;

	unsigned char opcode = (unsigned char)data[0] & 0x0F;
	unsigned char fin = (data[0] >> 7) & 0x01;
	unsigned char masked = (bool)((data[1] >> 7) & 0x01);

	unsigned int length = (unsigned char)(data[1] & (~0x80));

	if(length <= 125) {
			payloadSize = length;
	}
	else if(length == 126) {
			payloadSize = (unsigned char)(data[2] + (data[3]<<8));
			dataStart += 2;
	}
	else if(length == 127) {
			payloadSize = (unsigned char)(data[2] + (data[3]<<8));
			dataStart += 8;
	}

	// This becomes our writable buffer, for unmasking purposes
	unsigned char *payload = new unsigned char[payloadSize+1]; // +1 to make room for null termination

	if(masked) 
	{
		unsigned int mask = *((unsigned int*)(data+dataStart));
		dataStart += 4;

		memcpy(payload, data+dataStart, payloadSize);

		unsigned char* c = payload;

		for(unsigned int i = 0; i < payloadSize; i++)
			c[i] = c[i] ^ ((unsigned char*)(&mask))[i%4];
	}
	else
	{
		memcpy(payload, data+dataStart, payloadSize);
	}

	payload[payloadSize] = 0; // Null terminate!

	switch(opcode)
	{
	case TextFrame:
		{
			std::string reply = "Hello from server";

			NetPacket *packet = new NetPacket;

			packet->PacketData.write((unsigned char*)reply.data(), reply.size());
			packet->setPacketSize(reply.size());

			EncodeAsFrame(packet, TextFrame);

			Network->sendtoSocket(httpSession->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());

			delete packet;
		}
		break;
	case BinaryFrame:
		{
			/** This is the server parsing the received binary packet.
				The packets payload gets rewritten with the parsed payload + size,
				and then processed using our known packet structures.
			**/
			NetPacket *packet = new NetPacket;

			packet->PacketData.write(payload, payloadSize);

			// Re-process the packet using TCP-style buffers
			PacketManager->processPacketReceivedEvent(packet, httpSession);

			delete packet;
		}
		break;
	case CloseFrame:
		{
			// According to RFC6455, the server MUST reply to close frames as quickly as possible
			// The first 2 bytes received are, in network byte order, the code used to describe why the close frame
			// was sent. If there are more bytes after the first 2, then the following bytes describe the 'reason'
			// When replying, it is customary to reply with the error code that was received, if there was one
			Console->printf(Networking, "Received close frame\n");

			NetPacket *packet = new NetPacket;

			packet->setPacketSize(0);			

			if(payloadSize >= 2)
			{
				union {
					unsigned short i;
					char c[2];
				} val;

				val.c[0] = payload[0];
				val.c[1] = payload[1];

				unsigned short code = ntohs(val.i);

				Console->printf(Networking, "Closing code: %d - %s\n", code, ErrorCodeToString((FrameErrorCode)code).c_str());

				packet->PacketData << val.i;
				packet->setPacketSize(2);

				if(payloadSize > 2)
				{
					std::stringstream sbuffer;
					sbuffer << payload;

					Console->printf(Networking, "Closing reason: %s\n", sbuffer.str().substr(2).c_str());
				}
			}

			EncodeAsFrame(packet, CloseFrame);

			Network->sendtoSocket(httpSession->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());

			// Signal that this connection needs to shut down
			httpSession->ConnectionState = ConnectionStateType::Closed;

			delete packet;
		}
		break;
	case PingFrame:
		Console->printf(Networking, "Received ping frame\n");
		break;
	case PongFrame:
		Console->printf(Networking, "Received pong frame\n");
		break;
	}

	delete[] payload;
}

void NetHTTPInterpreter::AuthenticateHandshake()
{
	HTTPRequest req(reinterpret_cast<const char*>(httpPacket->getPacketBuffer()));

	std::string accept = CalcAcceptKey(req.GetHeader("Sec-WebSocket-Key"));

	HTTPResponse response(101);
	response.SetHeader("Upgrade", "websocket");
	response.SetHeader("Connection", "Upgrade");
	response.SetHeader("Sec-WebSocket-Accept", accept);

	std::string buf = response.GetResponseBuffer();

	if(Network->sendtoSocket(httpSession->getSocket(), reinterpret_cast<const unsigned char*>(buf.c_str()), buf.length()) == NetPlatform::Success)
		httpSession->HandshakeComplete = true;

	delete httpPacket;
	delete this;
}

std::string NetHTTPInterpreter::CalcAcceptKey(std::string key)
{
	unsigned char hash[40] = {0};

	key.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11"); // Magic string

	sha1::calc(key.data(), key.size(), hash);

	return base64_encode(reinterpret_cast<const unsigned char*>(hash), 20); // Hash should be of length 20
}

bool NetHTTPInterpreter::HandleGetRequest(NetPacket *packet, NetSession *session)
{
	HTTPRequest req(reinterpret_cast<const char*>(packet->getPacketBuffer()));

	std::string url = req.GetUrl();

	if (url.compare("/") == 0) // Only a WebSocket handshake request should be asking for the root
		return false;
	
	std::string target = DocumentRoot;

	if (url.find(".jpg") != std::string::npos)
		target.append("/images");

	target.append(url);

	// Thread the actual file loading so it doesn't block the main thread, as file loading can be slow
	std::thread FileThread(&NetHTTPInterpreter::SendFileToSession, target, std::ref(session));
	FileThread.detach();

	return true;
}

void NetHTTPInterpreter::SendFileToSession(std::string path, NetSession* session)
{
	std::ifstream file(path, std::ios::in | std::ios::binary | std::ios::ate);

	Console->printf(Networking, "Client %d requested url: %s\n", session->getSocket(), path.c_str());

	if (file.is_open())
	{
		unsigned int size = (unsigned int)file.tellg();
		char *data = new char[size];

		file.seekg(0, std::ios::beg);
		file.read(data, size);

		file.close();

		HTTPResponse response(200);

		if (path.find(".jpg") != std::string::npos)
			response.SetHeader("Content-Type", "image/jpg");
		else
			response.SetHeader("Content-Type", "pdf");

		response.SetHeader("Content-Length", std::to_string(size));
		response.SetContent(data, size);

		std::string buf = response.GetResponseBuffer();

		Network->sendtoSocket(session->getSocket(), (unsigned char*)buf.c_str(), buf.size());

		Console->printf(Debug, "Read %d bytes on file: %s\n", size, path.c_str());
	}
	else
	{
		HTTPResponse response(404);
		response.SetHeader("Content-Type", "text/html");
		response.SetHeader("Content-Length", std::to_string(9));
		response.SetContent("Not found");

		std::string buf = response.GetResponseBuffer();

		Network->sendtoSocket(session->getSocket(), (unsigned char*)buf.c_str(), buf.size());

		Console->printf(Debug, "Error 404 on file: %s\n", path.c_str());
	}
}

void NetHTTPInterpreter::EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode)
{
	const unsigned char *buffer = packet->getPacketBuffer();
	unsigned char header[16];
	int pos = 0;
	unsigned int size = packet->getPacketSize();

	header[pos++] = (unsigned char)(0x80 + opcode);

	if(size <= 125)
		header[pos++] = size;
	else if(size <= 65535)
	{
		header[pos++] = 126;
		header[pos++] = (size >> 8) & 0xFF;
		header[pos++] = size & 0xFF;
	}
	else
	{
		int64_t dsize = size;
		header[pos++] = 127;
		header[pos++] = (dsize >> 56) & 0xFF;
		header[pos++] = (dsize >> 48) & 0xFF;
		header[pos++] = (dsize >> 40) & 0xFF;
		header[pos++] = (dsize >> 32) & 0xFF;
		header[pos++] = (size >> 24) & 0xFF;
		header[pos++] = (size >> 16) & 0xFF;
		header[pos++] = (size >> 8) & 0xFF;
		header[pos++] = size & 0xFF;
	}

	unsigned int packetSize = pos + size;
	unsigned char *data = new unsigned char[packetSize];
	memcpy(data, header, pos);
	memcpy(data+pos, buffer, size);

	packet->PacketData.clear();
	packet->PacketData.write(data, packetSize);
	packet->setPacketSize(packetSize); // Update packet size
}

bool NetHTTPInterpreter::isHTTPPacket(NetPacket *packet)
{
	std::string buf = reinterpret_cast<const char*>(packet->getPacketBuffer());

	if (buf.length() >= 3)
		if (buf.substr(0, 3) == "GET")
			return true;
	return false;
}

std::string NetHTTPInterpreter::ErrorCodeToString(FrameErrorCode code)
{
	switch (code)
	{
		case NormalClosure:
			return "Normal closure";

		case GoingAway:
			return "Browser navigated away";

		case ProtocolError:
			return "Protocol error";

		case UnacceptableData:
			return "Unacceptable data";

		case Reserved:
		case Reserved2:
		case Reserved3:
			return "Reserved error";

		case InconsistentData:
			return "Inconsistent data";

		case PolicyViolation:
			return "Policy violation";

		case OversizedMessage:
			return "Oversized message";

		case ExtensionNegotiationFail:
			return "Negotiation failure";

		case UnexpectedCondition:
			return "Unexpected condition";

		default:
			return "Unknown error code";
	}
}