#include "PacketClientType.h"
#include <Network/NetPlatform.h>

PacketClientType::PacketClientType()
{
	packetID = pktClientType;
	packetSize = 3;
}

PacketClientType::PacketClientType(char clientType)
{
	packetID = pktClientType;

	this->ClientType = (ClientTypeEnum)clientType;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketClientType::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << (char)ClientType;
}

void PacketClientType::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);

	sess->getSessionState().setClientType(this->ClientType);
}

void PacketClientType::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	char type;

	ns >> type;

	this->ClientType = (ClientTypeEnum)type;
}