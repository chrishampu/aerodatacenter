#pragma once

#include "../NetPacket.h"

class PacketClientCommand : public NetPacket
{
public:
	PacketClientCommand();
	PacketClientCommand(unsigned int timestamp, unsigned char system, unsigned char sender, unsigned short len, const char *msg);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	void ParseCommand();

	const unsigned char *getMsg();
	unsigned short getMsgLen();

	unsigned int getTime() { return timestamp; }
	unsigned char getSystem() { return systemID; }
	unsigned char getSender() { return senderID; }

	enum SystemType {
		SystemDC = 0,
		SystemAT,
		SystemUAV
	};

private:
	unsigned int timestamp;
	unsigned char systemID;
	unsigned char senderID;
	unsigned short  msgLength;
	unsigned char msg[256];
};