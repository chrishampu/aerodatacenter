#include "PacketImageTransmit.h"

PacketImageTransmit::PacketImageTransmit()
{
	packetID = pktImageTransmit;
	packetSize = 2;
	dataSize = 0;
}

PacketImageTransmit::PacketImageTransmit(unsigned short width, unsigned short height, const char *imageData, unsigned int size)
{
	packetID = pktImageTransmit;

	imageWidth = width;
	imageHeight = height;
	dataSize = size;

	data = new char[size];

	memset(this->data, 0, size);
	memcpy(this->data, imageData, size);

	PreparePacket();
}

PacketImageTransmit::~PacketImageTransmit()
{
	if (dataSize > 0)
		delete[] data;
}

void PacketImageTransmit::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << imageWidth;
	PacketData << imageHeight;
	PacketData << dataSize;
	PacketData.write(reinterpret_cast<unsigned char*>(data), dataSize);

	setPacketSize(PacketData.bufSize());
}

void PacketImageTransmit::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketImageTransmit::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> dataSize;

	data = new char[dataSize];

	memset(this->data, 0, dataSize);

	ns.read((unsigned char*)data, dataSize);
}