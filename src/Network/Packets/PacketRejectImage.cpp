#include "PacketRejectImage.h"
#include <Logging/ReportGenerator.h>
#include <memory>

PacketRejectImage::PacketRejectImage()
{
	packetID = pktRejectImage;

	ImageID = -1;
	TargetID = -1;
}

void PacketRejectImage::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << ImageID;
	PacketData << TargetID;
}

void PacketRejectImage::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketRejectImage::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> ImageID;
	ns >> TargetID;
}
