#include "PacketAerialImage.h"
#include <Common/Platform.h>

PacketAerialImage::PacketAerialImage()
{
	packetID = pktAerialImage;
	packetSize = 2;

	timestamp = 0;
	imageID = 0;
	latitude = 0;
	longitude = 0;
	attitude = 0;
	heading = 0;
	pitchAngle = 0;
	rollAngle = 0;
	dataLength = 0;
	maxLon = 0;
	minLat = 0;
	minLon = 0;
	maxLat = 0;
	width = 0;
	height = 0;
	data = nullptr;
}

PacketAerialImage::PacketAerialImage(unsigned int timestamp, unsigned int imageID, float latitude, float longitude,
	float attitude, float heading, float pitchAngle, float rollAngle, unsigned int dataLength, float maxLon, float minLat,
	float minLon, float maxLat, unsigned int width, unsigned int height, const unsigned char *data)
{
	packetID = pktAerialImage;

	this->timestamp = timestamp;
	this->imageID = imageID;
	this->latitude = latitude;
	this->longitude = longitude;
	this->attitude = attitude;
	this->heading = heading;
	this->pitchAngle = pitchAngle;
	this->rollAngle = rollAngle;
	this->dataLength = dataLength;
	this->maxLon = maxLon;
	this->minLat = minLat;
	this->minLon = minLon;
	this->maxLat = maxLat;
	this->width = width;
	this->height = height;

	this->data = new unsigned char[dataLength+1];
	
	memset(this->data, 0, dataLength);

	memcpy(this->data, data, dataLength);

	this->data[dataLength] = 0; // Null terminate!

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

PacketAerialImage::~PacketAerialImage()
{
	if (data != nullptr)
		delete[] data;
}

void PacketAerialImage::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << timestamp;
	PacketData << imageID;
	PacketData << latitude;
	PacketData << longitude;
	PacketData << attitude;
	PacketData << heading;
	PacketData << pitchAngle;
	PacketData << rollAngle;
	PacketData << dataLength;
	PacketData << maxLon;
	PacketData << minLat;
	PacketData << minLon;
	PacketData << maxLat;
	PacketData << width;
	PacketData << height;
	PacketData << data;
}

void PacketAerialImage::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketAerialImage::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> timestamp;
	ns >> imageID;
	ns >> latitude;
	ns >> longitude;
	ns >> attitude;
	ns >> heading;
	ns >> pitchAngle;
	ns >> rollAngle;
	ns >> dataLength;
	ns >> maxLon;
	ns >> minLat;
	ns >> minLon;
	ns >> maxLat;
	ns >> width;
	ns >> height;

	data = new unsigned char[dataLength];

	ns.read(data, dataLength); // Read in the message

	Console->printf(Debug, "PacketAerialImage: Time: %d\n", timestamp);
}
