#include "PacketOnboardImage.h"
#include <Common/Platform.h>

PacketOnboardImage::PacketOnboardImage()
{
	packetID = pktOnboardImage;
	packetSize = 2;

	timestamp = 0;
	latitude = 0;
	longitude = 0;
	altitude = 0;
	pitchAngle = 0;
	rollAngle = 0;
	width = 0;
	height = 0;
	dataLength = 0;
	data = nullptr;
}

PacketOnboardImage::PacketOnboardImage(int timestamp, float latitude, float longitude, float altitude, float pitchAngle, float rollAngle,
										int width, int height, unsigned int dataLength, unsigned char *data)
{
	packetID = pktOnboardImage;

	this->timestamp = timestamp;
	this->latitude = latitude;
	this->longitude = longitude;
	this->altitude = altitude;
	this->pitchAngle = pitchAngle;
	this->rollAngle = rollAngle;
	this->dataLength = dataLength;
	this->width = width;
	this->height = height;

	this->data = new unsigned char[dataLength+1];
	
	memset(this->data, 0, dataLength);

	memcpy(this->data, data, dataLength);

	this->data[dataLength] = 0; // Null terminate!

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

PacketOnboardImage::~PacketOnboardImage()
{
	if (data != nullptr)
		delete[] data;
}

void PacketOnboardImage::PreparePacket()
{
	WritePacketHeader(packetID);

	// This packet doesn't get sent
}

void PacketOnboardImage::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketOnboardImage::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> timestamp;
	ns >> latitude;
	ns >> longitude;
	ns >> altitude;
	ns >> pitchAngle;
	ns >> rollAngle;
	ns >> width;
	ns >> height;
	ns >> dataLength;

	data = new unsigned char[dataLength];

	ns.read(data, dataLength); // Read in the message

	Console->printf(Debug, "PacketOnboardImage received. Time: %d, Width: %d, Height: %d\n", timestamp, width, height);
}
