#pragma once

#include "../NetPacket.h"

// This packet should only be received, and not sent
class PacketRecognitionValues : public NetPacket
{
public:
	PacketRecognitionValues();
	PacketRecognitionValues(unsigned char gaussian, unsigned char cannyLow, unsigned char cannyHigh, unsigned char houghVote,
		unsigned char houghLength, unsigned char houghDistance);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	unsigned char gaussian;
	unsigned char cannyLow;
	unsigned char cannyHigh;
	unsigned char houghVote;
	unsigned char houghLength;
	unsigned char houghDistance;
};