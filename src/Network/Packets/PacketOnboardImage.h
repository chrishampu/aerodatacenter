#pragma once

#include "../NetPacket.h"

class PacketOnboardImage : public NetPacket
{
public:
	PacketOnboardImage();
	PacketOnboardImage(int timestamp, float latitude, float longitude, float altitude, float pitchAngle, float rollAngle,
		int width, int height, unsigned int dataLength, unsigned char *data);

	~PacketOnboardImage();

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	int timestamp;
	float latitude;
	float longitude;
	float altitude;
	float pitchAngle;
	float rollAngle;
	int width;
	int height;
	unsigned int dataLength;
	unsigned char *data;
};