#pragma once

#include "../NetPacket.h"

class PacketImageTransmit : public NetPacket
{
public:
    PacketImageTransmit();
	PacketImageTransmit(unsigned short width, unsigned short height, const char *imageData, unsigned int size);
	~PacketImageTransmit();

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns, NetSession* sess);

    virtual void Decode(NetStream& ns);

	unsigned short getImageWidth() { return imageWidth; }
	unsigned short getImageHeight() { return imageHeight; }
	const char *getImageData() { return data; }
	unsigned int getImageSize() { return dataSize; }

private:
	unsigned short imageWidth;
	unsigned short imageHeight;
	unsigned int dataSize;
	char *data;
};