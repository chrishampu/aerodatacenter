#pragma once

#include <Common/Platform.h>
#include "../NetPacket.h"

class PacketImageTarget : public NetPacket
{
public:
	PacketImageTarget();
	PacketImageTarget(unsigned int imageID, unsigned int targetID, float latitude, float longitude, unsigned int minX, 
		unsigned int minY, unsigned int maxX, unsigned int maxY, unsigned char humanReview, bool CVFound, bool HumanFound, 
		unsigned int jsonLen, const char *json);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	enum HumanReviewStatus {
		None = 0,
		Accepted,
		Rejected
	};

	unsigned int imageID;
	unsigned int targetID;
	float latitude;
	float longitude;
	unsigned int minX;
	unsigned int minY;
	unsigned int maxX;
	unsigned int maxY;
	unsigned char humanReview;
	bool CVFound;
	bool HumanFound;
	unsigned int jsonLen;
	char json[256];
};
