#include "PacketImageTarget.h"
#include <Common/Platform.h>

PacketImageTarget::PacketImageTarget()
{
	packetID = pktImageTarget;
	packetSize = 2;

	imageID = 0;
	targetID = 0;
	latitude = 0;
	longitude = 0;
	minX = 0;
	minY = 0;
	maxX = 0;
	maxY = 0;
	humanReview = 0;
	CVFound = 0;
	HumanFound = 0;
	jsonLen = 0;
}

PacketImageTarget::PacketImageTarget(unsigned int imageID, unsigned int targetID, float latitude, float longitude, unsigned int minX,
	unsigned int minY, unsigned int maxX, unsigned int maxY, unsigned char humanReview, bool CVFound, bool HumanFound,
	unsigned int jsonLen, const char *json)
{
	packetID = pktImageTarget;

	memset(&this->json, 0, 256);

	if (jsonLen > 256)
	{
		Console->printf(Error, "Malformed target packet received\n");
		return;
	}
		

	memcpy(&this->json, json, jsonLen);
	this->json[jsonLen] = 0;

	this->imageID = imageID;
	this->targetID = targetID;
	this->latitude = latitude;
	this->longitude = longitude;
	this->minX = minX;
	this->minY = minY;
	this->maxX = maxX;
	this->maxY = maxY;
	this->humanReview = humanReview;
	this->CVFound = CVFound;
	this->HumanFound = HumanFound;
	this->jsonLen = jsonLen;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketImageTarget::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << imageID;
	PacketData << targetID;
	PacketData << latitude;
	PacketData << longitude;
	PacketData << minX;
	PacketData << minY;
	PacketData << maxX;
	PacketData << maxY;
	PacketData << (char)humanReview;
	PacketData << (char)CVFound;
	PacketData << (char)HumanFound;
	PacketData << jsonLen;
	PacketData << json;
}

void PacketImageTarget::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketImageTarget::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header
	unsigned char val;

	ns >> imageID;
	ns >> targetID;
	ns >> latitude;
	ns >> longitude;
	ns >> minX;
	ns >> minY;
	ns >> maxX;
	ns >> maxY;
	ns >> humanReview;
	ns >> val;
	CVFound = val&1;
	ns >> val;
	HumanFound = val&1;
	ns >> jsonLen;

	memset(&json, 0, 256); // Initialize the array

	if (jsonLen > 256) // Sanity check
		return;

	ns.read(reinterpret_cast<unsigned char*>(json), jsonLen); // Read in the message
	json[jsonLen] = 0;

	Console->printf(Debug, "PacketImageTarget: Img: %d, TargetID: %d\n", imageID, targetID);
}
