#include "PacketClientCommand.h"
#include <Network/NetPlatform.h>
#include <Network/NetSessionManager.h>
#include <Network/NetPacketManager.h>
#include <Database/DatabaseManager.h>

#ifdef PLATFORM_WIN
AddConsoleFunction(route_command, 0, 0)
{
	if (SessionManager->getAllSessions().size() == 0)
		return;

	PacketClientCommand *pack = new PacketClientCommand((unsigned int)std::time(NULL), PacketClientCommand::SystemAT, 
														PacketClientCommand::SystemUAV, 8, "Hello AT");

	PacketManager->processPacketReceivedEvent(pack, SessionManager->getAllSessions().begin()->second);

	delete pack;
}
#endif

PacketClientCommand::PacketClientCommand()
{
	packetID = pktClientCommand;
	packetSize = 2;
	timestamp = 0;
	senderID = 0;
	systemID = 0;
	msgLength = 0;
}

PacketClientCommand::PacketClientCommand(unsigned int timestamp, unsigned char system, unsigned char sender, unsigned short len, const char *msg)
{
	packetID = pktClientCommand;

	memset(&this->msg, 0, 256);
	memcpy(&this->msg, msg, len);

	this->msgLength = len;
	this->timestamp = timestamp;
	this->systemID = system;
	this->senderID = sender;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketClientCommand::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << timestamp;
	PacketData << systemID;
	PacketData << senderID;
	PacketData << msgLength;
	PacketData << msg;
}

void PacketClientCommand::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);

	bool accepted = false;

	Console->printf(Debug, "ClientCommandMessage - Time: %d, Sender: %d, System: %d, Len: %d, Message: %s\n", timestamp,
		senderID, systemID, msgLength, msg);

	switch (systemID)
	{
		case SystemUAV:
		case SystemAT:
			{
				PacketClientCommand *pack = new PacketClientCommand(timestamp, systemID,
					senderID, msgLength, reinterpret_cast<char*>(msg));

				NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

				for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
				{
					if (itr->second->getSessionState().getClientType() == systemID)
					{
						Network->sendtoSocket(itr->second->getSocket(), pack->PacketData.getBuffer(), pack->getPacketSize());
						Console->printf(Debug, "ClientCommandMessage: Relayed to %s system\n", systemID==SystemAT?"AT":"UAV");
						accepted = true;
					}
				}
				delete pack;
			}
			break;
		case SystemDC:
			ParseCommand();
			break;
	}

	Databases[Aero]->Execute("INSERT INTO `systemcommand` (Command, Parameters, DateTime, Accepted,SenderID, RecipientID) VALUES ('%s', '%s', %d, %d, %d, %d)",
								msg, "", timestamp, accepted, senderID, systemID);
}

void PacketClientCommand::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> timestamp;
	ns >> systemID;
	ns >> senderID;
	ns >> msgLength; // Retrieve the length of our message

	memset(&msg, 0, 256); // Initialize the array

	if (msgLength > 256) // Sanity check
		return;

	ns.read(msg, msgLength); // Read in the message
}

void PacketClientCommand::ParseCommand()
{
	// Parse DC-oriented command
}

const unsigned char *PacketClientCommand::getMsg()
{
	return msg;
}

unsigned short PacketClientCommand::getMsgLen()
{
	return msgLength;
}
