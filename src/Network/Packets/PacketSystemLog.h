#pragma once

#include "../NetPacket.h"

class PacketSystemLog : public NetPacket
{
public:
	PacketSystemLog();
	PacketSystemLog(int timestamp, char level, char system, unsigned short len, const char *msg);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	const unsigned char *getMsg();
	unsigned short getMsgLen();

	unsigned int getTime() { return timestamp; }
	unsigned char getLevel() { return level; }
	unsigned char getSystem() { return systemID; }

	enum SystemIDEnum{
		DC = 0,
		AT,
		UAV,
		WEB
	};

	enum LevelEnum {
		LevelDebug = 0,
		LevelInfo,
		LevelWarning,
		LevelCritical,
		LevelSent
	};

private:
	int timestamp;
	char level;
	char systemID;
	unsigned short  msgLength;
	unsigned char msg[256];
};