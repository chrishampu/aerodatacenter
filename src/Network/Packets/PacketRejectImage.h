#pragma once

#include "../NetPacket.h"

class PacketRejectImage : public NetPacket
{
public:
	PacketRejectImage();

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	signed int ImageID;
	signed int TargetID;
};