#ifndef _PACKET_TRANSMITREPORT_H_
#define _PACKET_TRANSMITREPORT_H_

#include "../NetPacket.h"

class PacketTransmitReport : public NetPacket
{
public:
    PacketTransmitReport();
    PacketTransmitReport(const char *msg, int len);

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns, NetSession* sess);

    virtual void Decode(NetStream& ns);

    unsigned char msg[256];
    unsigned short msglen;
};

#endif