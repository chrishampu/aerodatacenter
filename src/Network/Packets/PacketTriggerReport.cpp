#include "PacketTriggerReport.h"
#include <Logging/ReportGenerator.h>
#include <memory>

PacketTriggerReport::PacketTriggerReport()
{
	packetID = pktTriggerReport;

}

void PacketTriggerReport::PreparePacket()
{
	WritePacketHeader(packetID);

}

void PacketTriggerReport::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);

	ReportGenerator::CreateAeroReport();
}

void PacketTriggerReport::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

}
