#pragma once

#include "../NetPacket.h"

class PacketTriggerReport : public NetPacket
{
public:
	PacketTriggerReport();

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);
};