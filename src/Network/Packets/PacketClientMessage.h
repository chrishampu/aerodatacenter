#pragma once

#include "../NetPacket.h"

class PacketClientMessage : public NetPacket
{
public:
    PacketClientMessage();
    PacketClientMessage(const char *msg, int len);

    virtual void PreparePacket();

    virtual void Handle(NetStream& ns, NetSession* sess);

    virtual void Decode(NetStream& ns);

    const unsigned char *getMsg();
    unsigned short getMsgLen();

private:
    unsigned char msg[256];
    unsigned short msglen;
};