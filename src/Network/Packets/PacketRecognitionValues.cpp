#include "PacketRecognitionValues.h"

PacketRecognitionValues::PacketRecognitionValues()
{
	packetID = pktRecognitionValues;
}

PacketRecognitionValues::PacketRecognitionValues(unsigned char gaussian, unsigned char cannyLow, unsigned char cannyHigh, unsigned char houghVote,
	unsigned char houghLength, unsigned char houghDistance)
{
	packetID = pktRecognitionValues;

	this->gaussian = gaussian;
	this->cannyLow = cannyLow;
	this->cannyHigh = cannyHigh;
	this->houghVote = houghVote;
	this->houghLength = houghLength;
	this->houghDistance = houghDistance;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketRecognitionValues::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << gaussian;
	PacketData << cannyLow;
	PacketData << cannyHigh;
	PacketData << houghVote;
	PacketData << houghLength;
	PacketData << houghDistance;
}

void PacketRecognitionValues::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketRecognitionValues::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> gaussian;
	ns >> cannyLow;
	ns >> cannyHigh;
	ns >> houghVote;
	ns >> houghLength;
	ns >> houghDistance;
}