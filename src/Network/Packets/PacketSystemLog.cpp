#include "PacketSystemLog.h"
#include <Network/NetPlatform.h>
#include <Network/NetHTTPInterpreter.h>
#include <Database/DatabaseManager.h>

#ifdef PLATFORM_WIN
AddConsoleFunction(msglog, 5, 5)
{
	char **args = new char*[256];

	unsigned int count = GetArgs(args, payload, maxc);

	PacketSystemLog *packet = new PacketSystemLog(atoi(args[0]), atoi(args[1]), atoi(args[2]), atoi(args[3]), args[4]);

	NetHTTPInterpreter::EncodeAsFrame(packet, BinaryFrame);

	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	for (NetSessionManager::SessionMap::iterator it = sessions.begin(); it != sessions.end(); ++it)
		Network->sendtoSocket(it->second->getSocket(), packet->getPacketBuffer(), packet->getPacketSize());

	delete[] args;
}
#endif

PacketSystemLog::PacketSystemLog()
{
	packetID = pktSystemLog;
	packetSize = 2;
	timestamp = 0;
	level = 0;
	systemID = 0;
	msgLength = 0;
}

PacketSystemLog::PacketSystemLog(int timestamp, char level, char system, unsigned short len, const char *msg)
{
	packetID = pktSystemLog;

	memset(&this->msg, 0, 256);
	memcpy(&this->msg, msg, len);

	this->msgLength = len;
	this->timestamp = timestamp;
	this->level = level;
	this->systemID = system;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketSystemLog::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << timestamp;
	PacketData << level;
	PacketData << systemID;
	PacketData << msgLength;
	PacketData << msg;
}

void PacketSystemLog::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);

	PacketSystemLog *pack = new PacketSystemLog(timestamp, level,
		systemID, msgLength, reinterpret_cast<char*>(msg));

	NetHTTPInterpreter::EncodeAsFrame(pack, BinaryFrame);

	NetSessionManager::SessionMap sessions = SessionManager->getAllSessions();

	// Relay log message to system analyst clients
	for (NetSessionManager::SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
		if (itr->second->getSessionState().getClientType() == SystemAnalyst)
			Network->sendtoSocket(itr->second->getSocket(), pack->PacketData.getBuffer(), pack->getPacketSize());

	delete pack;

	Databases[Aero]->Execute("INSERT INTO `systemlogs` (DateTime, SystemID, Level, Message) VALUES (%d, %d, %d, %s)", timestamp, systemID, level, msg);
}

void PacketSystemLog::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> timestamp;
	ns >> level;
	ns >> systemID;
	ns >> msgLength; // Retrieve the length of our message

	memset(&msg, 0, 256); // Initialize the array

	if (msgLength > 256) // Sanity check
		return;

	ns.read(msg, msgLength); // Read in the message

	Console->printf(Debug, "SystemLogMessage - Time: %d, Level: %d, ID: %d, Len: %d, Message: %s\n", timestamp,
		level, systemID, msgLength, msg);
}

const unsigned char *PacketSystemLog::getMsg()
{
	return msg;
}

unsigned short PacketSystemLog::getMsgLen()
{
	return msgLength;
}
