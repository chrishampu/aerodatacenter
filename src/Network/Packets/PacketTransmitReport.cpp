#include "PacketTransmitReport.h"

PacketTransmitReport::PacketTransmitReport()
{
	packetID = pktTransmitReport;
	packetSize = 2;
	msglen = 0;
}

PacketTransmitReport::PacketTransmitReport(const char *msg, int len)
{
	packetID = pktTransmitReport;

	memset(&this->msg, 0, 256);
	memcpy(&this->msg, msg, len);

	msglen = len;
	setPacketSize(msglen + 2 + 2); // msg size + msglen + packetID

	PreparePacket();
}

void PacketTransmitReport::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << msglen;
	PacketData << msg;
}

void PacketTransmitReport::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);
}

void PacketTransmitReport::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	ns >> msglen; // Retrieve the length of our message

	memset(&msg, 0, 256); // Initialize the array

	if(msglen > 256) // Sanity check
		return;

	ns.read(msg, msglen); // Read in the message
}