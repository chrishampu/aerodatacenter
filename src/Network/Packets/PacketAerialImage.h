#pragma once

#include "../NetPacket.h"

class PacketAerialImage : public NetPacket
{
public:
	PacketAerialImage();
	PacketAerialImage(unsigned int timestamp, unsigned int imageID, float latitude, float longitude,
		float attitude, float heading, float pitchAngle, float rollAngle, unsigned int dataLength, float maxLon, float minLat,
		float minLon, float maxLat, unsigned int width, unsigned int height, const unsigned char *data);

	~PacketAerialImage();

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	unsigned int timestamp;
	unsigned int imageID;
	float latitude;
	float longitude;
	float attitude;
	float heading;
	float pitchAngle;
	float rollAngle;
	unsigned int dataLength;
	float maxLon;
	float minLat;
	float minLon;
	float maxLat;
	unsigned int width;
	unsigned int height;
	unsigned char *data;
};