#pragma once

#include "../NetPacket.h"

enum ClientTypeEnum {
	ClientUnknown = -1,
	ClientDC,
	AntennaTracker,
	ClientUAV,
	ImageAnalyst,
	SystemAnalyst,
	Recon,
	Spectator,
	Judge,
};

class PacketClientType : public NetPacket
{
public:
	PacketClientType();
	PacketClientType(char clientType);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	char getClientType() { return ClientType; }

private:
	ClientTypeEnum ClientType;
};