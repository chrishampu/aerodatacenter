#pragma once

#include <sstream>
#include <map>

class HTTPRequest
{
public:
	HTTPRequest();
	HTTPRequest(std::string buffer);

	~HTTPRequest();

	void SetUrl(std::string url);
	void SetRequestMethod(std::string method);
	void SetHeader(std::string header, std::string value);

	void ConsumeBuffer(std::string buffer);

	std::string GetHeader(std::string header);
	std::string GetUrl();
	std::string GetMethod();

private:
	typedef std::map<std::string, std::string> HeaderMapT;
	HeaderMapT HeaderMap;

	std::string Method;
	std::string Url;

public:
	HeaderMapT GetAllHeaders() { return HeaderMap; }
};