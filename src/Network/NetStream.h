#pragma once

#undef  SAFE_DELETE
#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL; // Avoiding null pointers

#undef  SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(a) if( (a) != NULL ) delete [] (a); (a) = NULL; // Also avoiding null pointers

#include <stdio.h>
#include <iostream>
#include <string.h> // memcpy, memmove

/**
	Buffer is a class that is essentially a dynamically growing buffer.
	As data is placed in the buffer, the size of the buffer expands accordingly
	based on the data type. The usage of this buffer will be to store information
	used for sending/recieving packets in true C++ style.

	Eventually C++ streams will replace a lot of the current C style strings used
	in the program.
*/

template<typename T, int _StorSize = 256>
class Buffer
{
protected:
	T* data;

	unsigned int dataStart;
	unsigned int dataEnd;

	unsigned int allocSize;

	void alloc(const unsigned int& size) 
	{
		if (size == 0) 
		{
			// Clear everything
			SAFE_DELETE_ARRAY(data)
//			delete[] data;
			allocSize = 0;
			dataStart = 0;
			dataEnd = 0;
			return;
		}

		unsigned int r = size % _StorSize;

		// What's the size of the new buffer?
		unsigned int newsize = size;
		if (r > 0)
			newsize += _StorSize - r; // Make is a multiple of _StorSize

		// No need to grow the memory buffer.
		if (newsize == allocSize)
		{
			// Does the data fits in the current buffer?
			if (dataStart + size <= allocSize) {
				// The data fits. No need for panic.
				return;
			}

			// Moving the useful data to the beggining of the buffer...
			memmove(data, data+dataStart, bufSize());
			dataEnd = bufSize();
			dataStart = 0;
			return;
		}

		T* newdata = new T[newsize];
		if (data != NULL) 
		{
			// How much are we going to copy?
			unsigned int copysize = (bufSize() > size)?size:bufSize();
			dataEnd = copysize;
			if (copysize > 0)
				memcpy(newdata, data + dataStart, copysize);
		}

		SAFE_DELETE_ARRAY(data)
//		delete[] data;
		
		data = newdata;
		allocSize = newsize;
		dataStart = 0;
	}

public:
	Buffer()
	{
		data = NULL;
		dataStart = 0;
		dataEnd = 0;
	}

	~Buffer()
	{
		clear();
	}

	void clear()
	{
		alloc(0); // Heap corruption if NULL checks aren't done on delete pointers
	}
	
	// How much data is in our buffer?
	unsigned int bufSize() const
	{
		return (dataEnd - dataStart);
	}

	// Move the cursor a certain amount without actually
	// reading the data
	void skip(const unsigned int& size) {
		dataStart += size;
		if (dataStart > dataEnd)
			dataStart = dataEnd;
	}

	// Read ahead a certain amount of data in the buffer without
	// moving the cursor location
	unsigned int peek(T* dest, const unsigned int& size) const
	{
		unsigned int ret = size;
		if (size > bufSize())
			ret = bufSize();
		if (ret == 0)
			return(0);

		memcpy(dest, data + dataStart, ret);

		return(ret);
	}

	// We can return a C-style version of our buffer for compatibility
	const T* getBuffer()
	{
		const T* ret = data;
		ret += dataStart;

		return(ret);
	}

	unsigned int write(const T* src, int size) {
		alloc(bufSize() + size);
		memcpy(data + dataEnd, src, size);
		dataEnd += size;
		return(size);
	}

	unsigned int write(std::istream& s) 
	{
		T buf[_StorSize];
		int size = 0;

		while (!s.eof())
		{
			buf[size++] = s.get();

			if (size >= _StorSize)
			{
				write(buf, size);
				size = 0;
			}
		}

		if (size > 0)
			write(buf, size);

		return(size);
	}

	unsigned int write(std::istream& s, const unsigned int& size) 
	{
		T buf[_StorSize];
		int k = 0;
		unsigned int wrote = 0;

		while (!s.eof() && (wrote <= size)) 
		{
			buf[k++] = s.get();
			wrote++;

			if (k >= _StorSize)
			{
				write(buf, k);
				k = 0;
			}
		}

		if (k > 0)
			write(buf, k);

		return(wrote);
	}

	unsigned int read(T* dest, const unsigned int& size) {
		unsigned int ret = size;
		if (size > bufSize())
			ret = bufSize();
		if (ret == 0)
			return(0);

		memcpy(dest, data + dataStart, ret);
		dataStart += size;
		alloc(bufSize());
		return(ret);
	}

	unsigned int read(std::ostream& dest)
	{
		int ret = bufSize();

		dest.write((char*)data + dataStart, ret);
		alloc(0);

		return(ret);
	}

	Buffer& operator >> (std::ostream& s)
	{
		s.write((char*)data + dataStart, bufSize());
		alloc(0);

		return(*this);
	}

	Buffer& operator << (std::istream& s)
	{
		write(s);
		return(*this);
	}

	Buffer& operator << (const char *s)
	{
		if(strlen(s) == 0 || s == NULL)
			return(*this);

		write(reinterpret_cast<const unsigned char*>(s), strlen(s));
		return(*this);
	}

	Buffer& operator << (const unsigned char *s)
	{
		if(strlen((const char *)s) == 0 || s == NULL)
			return(*this);

		write(s, strlen((const char *)s));
		return(*this);
	}

	Buffer& operator << (const char s)
	{
		write((unsigned char*)&s, sizeof(char));
		return(*this);
	}

	Buffer& operator << (const unsigned char s)
	{
		write((unsigned char*)&s, sizeof(char));
		return(*this);
	}

	Buffer& operator << (const unsigned int c)
	{
		if(c < 0)
			return(*this);

		write((unsigned char*)&c, sizeof(unsigned int));
		return(*this);
	}

	Buffer& operator << (const int c)
	{
		write((unsigned char*)&c, sizeof(int));
		return(*this);
	}

	Buffer& operator << (const double c)
	{
		write((unsigned char*)&c, sizeof(double));
		return(*this);
	}

	Buffer& operator << (const float c)
	{
		write((unsigned char*)&c, sizeof(float));
		return(*this);
	}

	Buffer& operator << (const unsigned short c)
	{
		if(c < 0)
			return(*this);

		write((unsigned char*)&c, sizeof(unsigned short));
		return(*this);
	}

	Buffer& operator << (const short c)
	{
		write((unsigned char*)&c, sizeof(short));
		return(*this);
	}

	Buffer& operator >> (unsigned char& c) 
	{
		if (bufSize() == 0)
			return(*this);

		c = *(data + dataStart);

		dataStart++;

		return(*this);
	}

	Buffer& operator >> (short& c) 
	{
		c = 0;
		if (bufSize() < sizeof(short))
			return(*this);

		c = *(short*)(data + dataStart);
		dataStart += sizeof(short);

		return(*this);
	}

	Buffer& operator >> (unsigned short& c)
	{
		c = 0;
		if (bufSize() < sizeof(unsigned short))
			return(*this);

		c = *(unsigned short*)(data + dataStart);
		dataStart += sizeof(unsigned short);

		return(*this);
	}

	Buffer& operator >> (int& c)
	{
		c = 0;
		if (bufSize() < sizeof(int))
			return(*this);

		c = *(int*)(data + dataStart);
		dataStart += sizeof(int);

		return(*this);
	}

	Buffer& operator >> (unsigned int& c)
	{
		c = 0;
		if (bufSize() < sizeof(unsigned int))
			return(*this);

		c = *(unsigned int*)(data + dataStart);
		dataStart += sizeof(unsigned int);

		return(*this);
	}

	Buffer& operator >> (double& c)
	{
		c = 0;
		if (bufSize() < sizeof(double))
			return(*this);

		c = *(double*)(data + dataStart);
		dataStart += sizeof(double);

		return(*this);
	}

	Buffer& operator >> (float& c)
	{
		c = 0;
		if (bufSize() < sizeof(float))
			return(*this);

		c = *(float*)(data + dataStart);
		dataStart += sizeof(float);

		return(*this);
	}

	Buffer& operator >> (char& c)
	{
		c = 0;
		if (bufSize() < sizeof(char))
			return(*this);

		c = *(unsigned int*)(data + dataStart);
		dataStart += sizeof(char);

		return(*this);
	}

};

// Declare NetStream as a Buffer so we no longer have to write NetStream<unsigned char>
// This makes the code a little easier to read, as well as now we can use Buffers in other
// places where Net would not be an appropriate prefix.
typedef Buffer<unsigned char> NetStream;