#include "NetSessionManager.h"

#ifdef PLATFORM_WIN
#define new DEBUG_CLIENTBLOCK
#endif

NetSessionManager *SessionManager = NULL;

NetSessionManager::NetSessionManager()
{

}

NetSessionManager::~NetSessionManager()
{
    for (SessionMap::iterator itr = Sessions.begin(), next; itr != Sessions.end(); itr = next)
    {
        next = itr;
        ++next;

        delete itr->second;
    }
	
	Sessions.clear();
}

void NetSessionManager::Create()
{
	if(SessionManager == NULL)
		SessionManager = new NetSessionManager();
}

void NetSessionManager::Destroy()
{
	if(SessionManager != NULL)
		delete SessionManager;
}

void NetSessionManager::UpdateSessions()
{
    for (SessionMap::iterator itr = Sessions.begin(), next; itr != Sessions.end(); itr = next)
    {
        next = itr;
        ++next;

        NetSession* pSession = itr->second;

        if (!pSession->Update())
        {
            Sessions.erase(itr);
            delete pSession;
        }
    }
}

NetSession* NetSessionManager::FindSession(NetSocket sock)
{
	SessionMap::const_iterator itr = Sessions.find(sock);

    if (itr != Sessions.end())
        return itr->second;
    else
        return NULL;
}

void NetSessionManager::AddSession(NetSession *session)
{
	Sessions[session->getSocket()] = session;
}

void NetSessionManager::RemoveSession(NetSession *session)
{
    SessionMap::const_iterator itr = Sessions.find(session->getSocket());

    if (itr != Sessions.end() && itr->second)
    {
		Console->printf(Networking, "Removing session %d\n", session->getSocket());
		delete itr->second;
		Sessions.erase(itr);
    }
}
