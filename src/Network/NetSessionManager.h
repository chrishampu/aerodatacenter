#pragma once

#include "NetSession.h"
#include <unordered_map>

class NetSessionManager
{
public:
	NetSessionManager();
	~NetSessionManager();

	void Create();
	void Destroy();

	void UpdateSessions();

	NetSession* FindSession(NetSocket sock);

	void AddSession(NetSession *session);
	void RemoveSession(NetSession *session);

	typedef std::unordered_map<NetSocket, NetSession*> SessionMap;
	SessionMap getAllSessions() { return Sessions; }

private:
	SessionMap Sessions;
};

extern NetSessionManager *SessionManager;