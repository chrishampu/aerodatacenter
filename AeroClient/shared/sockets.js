
    var SERVER_IP = "192.168.72.10";
    var SERVER_PORT = "24000"; 
    var IMAGE_EXTENSION = ".jpg"; 

    var system_enum = {
        0 : "DC",
        1 : "AT",
        2 : "UAV",
		3 : "WEB"
    };

    var level_enum = {
        0 : "DEBUG",
        1 : "INFO",
        2 : "WARNING",
        3 : "CRITICAL",
        4 : "SENT"
    };
	
	var client_type_enum = {
		"Unknown" : -1,
		"DC" : 0,
		"AntennaTracker" : 1,
		"UAV" : 2,
		"ImageAnalyst" : 3, 
		"SystemAnalyst" : 4,
		"Recon" : 5,
		"Spectator" : 6,
		"Judge" : 7,
	};

	var host = "ws://"+SERVER_IP+":"+SERVER_PORT, // Address
	socket, // Our socket object
	connected, // Keeping track of our state
	clientType, // Determines what the DataCenter will send us
	Packets = Object.freeze({ // Our packet structures/properties
		"RejectImage" : {
			opcode: 275,
			name: "RejectImage",
			recv: function(buffer) {
				// This should not be received by the clients; it is only sent;.
			}
		},
		"TransmitReport" : {
			opcode : 267,
			name: "TransmitReport",
			recv: function(buffer) {
				var len = buffer.peek(2, buffer.VarType["ushort"]);
				var message = buffer.peek(4, buffer.VarType["uchar"], len);
				
				// http://server:ip/ + message
				window.open('HTTP://' + SERVER_IP + ':' + SERVER_PORT + '/' + message, '_blank');
			}
		},
		"RecognitionValues" : { 
			opcode : 0x107,
			name: "RecognitionValues",
			recv: function(buffer) {
				var gaussian = buffer.peek(2, buffer.VarType["uchar"]);
				var cannylow = buffer.peek(3, buffer.VarType["uchar"]);
				var cannyhigh = buffer.peek(4, buffer.VarType["uchar"]);
				var houghvote = buffer.peek(5, buffer.VarType["uchar"]);
				var houghlength = buffer.peek(6, buffer.VarType["uchar"]);
				var houghdistance = buffer.peek(7, buffer.VarType["uchar"]);
				
				$( "#gaussian-val" ).html( gaussian );
				$( "#gaussian" ).slider('value', gaussian);
				
				$( "#canny-low-val" ).html( cannylow );
				$( "#canny-low" ).slider('value', cannylow);
				
				$( "#canny-high-val" ).html( cannyhigh );
				$( "#canny-high" ).slider('value', cannyhigh);
				
				$( "#hough-vote-val" ).html( houghvote );
				$( "#hough-vote" ).slider('value', houghvote);
				
				$( "#hough-length-val" ).html( houghlength );
				$( "#hough-length" ).slider('value', houghlength);
				
				$( "#hough-distance-val" ).html( houghdistance );
				$( "#hough-distance" ).slider('value', houghdistance);
			}
		},
		"ImageTarget" : { 
			opcode : 0x109,
			name: "ImageTarget",
			recv: function(buffer) {
				var imageID = buffer.peek(2, buffer.VarType["uint"]);
				var targetID = buffer.peek(6, buffer.VarType["uint"]);
				var latitude = buffer.peek(10, buffer.VarType["float"]); 
				var longitude = buffer.peek(14, buffer.VarType["float"]); 
				var minX = buffer.peek(18, buffer.VarType["uint"]);
				var minY = buffer.peek(22, buffer.VarType["uint"]);
				var maxX = buffer.peek(26, buffer.VarType["uint"]);
				var maxY = buffer.peek(30, buffer.VarType["uint"]);
				var humanReview = buffer.peek(34, buffer.VarType["uchar"], 1).charCodeAt(0);
				var CVFound = buffer.peek(35, buffer.VarType["uchar"], 1).charCodeAt(0);
				var HumanFound = buffer.peek(36, buffer.VarType["uchar"], 1).charCodeAt(0);
				var jsonLen = buffer.peek(37, buffer.VarType["uint"]);
				var json = buffer.peek(41, buffer.VarType["char"], jsonLen);

				disp.addNewTarget(new Target(targetID, imageID, latitude, longitude, minX, minY, maxX, maxY, json, CVFound));
			}
		},
		"AerialImage" : { 
			opcode : 0x106,
			name: "AerialImage",
			recv: function(buffer) {
               			var timestamp = buffer.peek(2, buffer.VarType["uint"]);
				var imageID = buffer.peek(6, buffer.VarType["uint"]);
				var latitude = buffer.peek(10, buffer.VarType["float"]); 
				var longitude = buffer.peek(14, buffer.VarType["float"]); 
				var attitude = buffer.peek(18, buffer.VarType["float"]); 
				var heading = buffer.peek(22, buffer.VarType["float"]); 
				var pitchAngle = buffer.peek(26, buffer.VarType["float"]); 
				var rollAngle = buffer.peek(30, buffer.VarType["float"]);
				var urlLength = buffer.peek(34, buffer.VarType["uint"]);
				var maxLon = buffer.peek(38, buffer.VarType["float"]); 
				var minLat = buffer.peek(42, buffer.VarType["float"]); 
				var minLon = buffer.peek(46, buffer.VarType["float"]); 
				var maxLat = buffer.peek(50, buffer.VarType["float"]);
				var height = buffer.peek(54, buffer.VarType["uint"]);
				var width = buffer.peek(58, buffer.VarType["uint"]);
				var url = buffer.peek(62, buffer.VarType["char"], urlLength);

				var src = "http://"+SERVER_IP+":"+SERVER_PORT+ "/" + url + ".jpg";
				
				disp.addNewImage(new AerialImage(imageID, latitude, longitude, minLat, minLon, maxLat, maxLon, url,
								width, height, src));
			}
		},
		"ClientType" : { 
			opcode : 0x105,
			name: "ClientType",
			recv: function(buffer) {
			}
		},
		"SystemLog" : { 
			opcode : 0x104,
			name: "SystemLog",
			recv: function(buffer) {
                var timestamp = buffer.peek(2, buffer.VarType["int"]); 
                var level = level_enum[buffer.peek(6, buffer.VarType["char"])]; 
                var system = system_enum[buffer.peek(7, buffer.VarType["char"])]; 
                var len = buffer.peek(8, buffer.VarType["ushort"]); 
                var message = buffer.peek(10, buffer.VarType["char"], len); 
                
                disp.addNewLogMessage(new Message(level, system, message, getTimestsamp(timestamp)));  
			}
		},
		"Message" : {  //for sending messages to the server from the client.
			opcode : 0x101,
			name: "ClientMessage",
			recv: function(buffer) {
				var len = buffer.peek(2, buffer.VarType["ushort"]);
				var message = buffer.peek(4, buffer.VarType["uchar"], len);
			}
		},
		/** Packet Structure
			short: OpCode
			short: PayloadSize
			byte * PayloadSize: Base64 Img Data
		**/
        "ImgData" : {
            opcode : 0x100,
            name: "ImageData", 
            recv: function(buffer) {

                //#TODO: change this to reflect new packet structures

				var width = buffer.peek(2, buffer.VarType["ushort"]);
				var height = buffer.peek(4, buffer.VarType["ushort"]);
				var size = buffer.peek(6, buffer.VarType["uint"]);
				var payload = buffer.peek(10, buffer.VarType["uchar"], size);
                var image_url = "http://"+SERVER_IP+":"+SERVER_PORT+"/"+payload+IMAGE_EXTENSION; 

                var img = new AerialImage(1,1,1,1,1,1,1,image_url, width, height); 

                //disp.add(img); 

                console.log('recieved image: '+img); 
            }
        }

	});

    connect = function(type, cb)
	{
		var callback = cb || undefined;
		connected = false;
		
		clientType = type === 'undefined' ? -1 : type;
		
		socket = new WebSocket(host);
		socket.binaryType = "arraybuffer";

		socket.onopen = function() {
			connected = true;
			
			if(callback !== undefined)
				callback(true);
			 
			//syncing packet packet_num: 0x105 <byte-enum-system-type>
			
			var buffer = new PacketBuffer(2 + 1);
			
			buffer.write(Packets["ClientType"].opcode, buffer.VarType["ushort"]);
			
			buffer.write(clientType, buffer.VarType["char"]);
		
			buffer.send();
			delete buffer;
		};
		
		socket.onmessage = function(msg) {
			if(msg.data instanceof ArrayBuffer)
			{
				processData(msg.data);
			}
			else
			{
				//logmessage('<p>Text received: ' + msg.data + '</p>');
			}
		};
		
		socket.onclose = function() {
			if(connected == false) {
				//logmessage('<p>Failed to connect to ' + host + '.</p>');
			}
			if(socket.readyState == 3) {
				//logmessage('<p>Socket connection is closed.</p>');
			}

            connected = false;
				
			if(callback !== undefined)
				callback(false);
		};			
	};

	logmessage = function (msg) {
		$('#logging').prepend(msg);
	};

	processData = function(data) {
		var buffer = new PacketBuffer(data);
		var op = buffer.peek(0, buffer.VarType["ushort"]);
		var processed = false;
		
		// We need to find the specific packet we're looking for
		for(var i in Packets)
		{
			if(op === Packets[i].opcode)
			{
				Packets[i].recv(buffer);
				processed = true;
			}
		}
		
		if(processed === false)
		{
			logmessage("Unknown packet #" + op + " received");
			// Optionally, we could also force this connection to close
		}
	};

	/** Packet Buffer API
		This class is multi functional. You can pass it a number value, and it'll create
		a fresh buffer, of that length, and begin writing to it. Or you can pass it
		an existing ArrayBuffer object, and it'll allow you to read from it.
	**/
	var PacketBuffer = function(data) {
		this.cursor = 0;
		this.writable = false;
	
		if(typeof data === 'number')
		{
			this.packetLength = data;
			this.buffer = new ArrayBuffer(this.packetLength);
			this.data = new DataView(this.buffer);
			this.writable = true;
		}
		else if(typeof data === 'object')
		{
			this.buffer = data;
			this.packetLength = this.buffer.byteLength;
			this.data = new DataView(this.buffer);
		}
	};	

	PacketBuffer.prototype.VarType = Object.freeze({
		"char" : { size : 1, name : "char" },
		"uchar" : { size : 1, name : "uchar" },
		"ushort" : { size : 2, name : "ushort" },
		"short" : { size : 2, name : "short" },
		"uint" : { size : 4, name : "uint" },
		"int" : { size : 4, name : "int" },
		"float" : { size : 4, name : "float" }
	});

	PacketBuffer.prototype.write = function(variable, type) {
		if(this.writable == false)
			return false;
		
		if(typeof variable === 'number')
		{
			switch(type.size)
			{
				case 1:
					if(type.name === "uchar")
						this.data.setUint8(this.cursor, variable, true);
					else
						this.data.setInt8(this.cursor, variable, true);
					break;
				
				case 2:
					if(type.name === "ushort")
						this.data.setUint16(this.cursor, variable, true);
					else
						this.data.setInt16(this.cursor, variable, true);
					break;
				
				case 4:
					if(type.name === "uint")
						this.data.setUint32(this.cursor, variable, true);
					else
						this.data.setInt32(this.cursor, variable, true);
					break;
			}
			this.cursor += type.size;
		}
		else if(typeof variable === 'string')
		{
			for(var i = 0; i < variable.length; i++)
				this.data.setUint8(this.cursor+i, variable.charCodeAt(i), true);
			this.cursor += variable.length;
		}
	};


	PacketBuffer.prototype.peek = function(offset, type, amount) {
		switch(type.name)
		{
			case "char":
				if(typeof amount === 'undefined')
					return this.data.getInt8(offset, true);
				else
				{
					// For now we'll assume that if a number of characters are requested, that
					// we should also encode it as a string object using 'fromCharCode()'
					var ret = "";
					for(var i = 0; i < amount; i++)
						ret += String.fromCharCode(this.data.getUint8(offset+i, true));
					return ret;
				}
				break;
			case "uchar":
				if(typeof amount === 'undefined')
					return this.data.getUint8(offset, true);
				else
				{
					var ret = "";
					for(var i = 0; i < amount; i++)
						ret += String.fromCharCode(this.data.getUint8(offset+i, true));
					return ret;
				}
				break;
			case "ushort":
				return this.data.getUint16(offset, true);
				break;
			case "short":
				return this.data.getInt16(offset, true);
				break;
			case "int":
				return this.data.getInt32(offset, true);
				break;
			case "uint":
				return this.data.getUint32(offset, true);
				break;
			case "float":
				return this.data.getInt32(offset, true);
				break;
		}
	};
	
	PacketBuffer.prototype.send = function() {
		socket.send(this.buffer);
	};	
	
	sendMessage = function(msg) {
		// Define a new PacketBuffer object, passing it a size
		// The size for this packet is 'opcode' + 'string length' + 'string'
		var buffer = new PacketBuffer(2 + 2 + msg.length);
		
		// Here we write the opcode as a ushort
		buffer.write(Packets["Message"].opcode, buffer.VarType["ushort"]);
		
		// Write the length of our string as a ushort
		buffer.write(msg.length, buffer.VarType["ushort"]);
		
		// Finally, write the actual string
		buffer.write(msg);
		
		// Send our buffer
		buffer.send();
		
		// And clean up
		delete buffer;
	};


