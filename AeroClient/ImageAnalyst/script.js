/*
* @date: 2014-04-10
* @author: Simon Diemert
*
* DEPENDANCIES: 
*   - sockets.js with appropriate target and image packets defined.
*   - kinectjs.min.js for drawing on the canvas (http://kineticjs.com/)
*/

/**
* This function definition will not just connect, but also pass a callback function
* which will be run when the connect function either successfully connects, or disconnects.
* This will adjust the UI correctly, and give the client control over the connection.
*/
do_connect = function()
{
	connect(client_type_enum["ImageAnalyst"], function(status) {
		if(status == false)
		{
			$('#connected').removeClass('status-connected').addClass('status-disconnected');
			$('#connected').html("Disconnected");
			$('#connect-btn').html("Connect");
		}
		else
		{
			$('#connected').removeClass('status-disconnected').addClass('status-connected');
			$('#connected').html("Connected");
			$('#connect-btn').html("Disconnect");
		}
	});
}

/*
*   Class that will contain all the data in to be displayed. 
*   Manages the display of the data in the HTML template.
*/
function Display()
{
    this.targets = new Array();  //holds all the target objects
    this.aerialImages = new Array();  //holds all the aerialImage types
	
	this.resetDisplay();

    //init the Kinetic stage
    this.stage = new Kinetic.Stage({
        container: 'canvas-container',
        height: 400, 
        width: 500 
    });
	
	// Pre-initialize acccordion
	$("#target-details").accordion({
		heightStyle: "fill",
		animate: false
	});
}

Display.prototype.resetDisplay = function()
{
	this.displayID = -1;
	this.refreshTable();
	
	$("#add-target-shape").addClass("input-disabled");
	$("#add-target-colour").addClass("input-disabled");
	$("#add-new-target-confirm").addClass("input-disabled");
}

/**
* Gets thes the image object from the aerialImages array identified by id.
*
* @param id : the id of the image to search for. 
*/
Display.prototype.getImageById = function(id){
    for(i in this.aerialImages){
        if(this.aerialImages[i].id === id){
            return this.aerialImages[i]; 
        }
    }
	return 'undefined';
}

Display.prototype.getIndexByImage = function(img)
{
    for(var i = 0; i < this.aerialImages.length; i++)
        if(this.aerialImages[i].id === img.id)
            return i; 
	return -1;
}

Display.prototype.getTargetById = function(id) {
    for(var i = 0; i < this.targets.length; i++)
        if(this.targets[i].target === id)
            return this.targets[i];
}

Display.prototype.getTargetsByImageId = function(id) {
	var out = new Array();
	
    for(var i = 0; i < this.targets.length; i++)
        if(this.targets[i].imageId === id)
            out.push(this.targets[i]);
			
	return out;
}

Display.prototype.removeTargetByTargetId = function(id) {
    for(var i = 0; i < this.targets.length; i++)
        if(this.targets[i].target === id)
            this.targets.splice(i, 1);
}

/**
* adds a new target to the display's target array.
* 
* @param t: the target object (see class below) to add
**/
Display.prototype.addNewTarget = function(t){
    this.targets.push(t); 
}

/**
* Clears all of the rows in the displays table and adds them all back
* in accroding to the set of currently defined filters.
*/
Display.prototype.refreshTable = function() {
	$("#verified-image-list-table tbody").html(""); 
	$("#new-image-list-table tbody").html(""); 
   
	for(var i = 0; i < this.aerialImages.length; i++)
        this.addAerialImageToTable(this.aerialImages[i]);
}

/**
* Adds a new image to the Display.aerialImages array. 
*
* @param img: the AerialImage object to store into the array. (see below) 
**/
Display.prototype.addNewImage = function(img) {
    this.aerialImages.push(img);  
    this.addAerialImageToTable(img);
}

Display.prototype.addAerialImageToTable = function(t){
    var s = "<tr data-image-id="+t.id+">"
		+ "<td>"+t.id+"</td>"
		+ "<td>"+t.url+"</td>"
		+ "</tr>";

    if(t.stat === "NEW"){
		$("#new-image-list-table tbody").append(s); 
    }else if(t.stat === "VERIFIED") {
        $("#verified-image-list-table tbody").prepend(s); 
    }

	var tag = "[data-image-id="+t.id+"]";
	
	$(tag).on("click", function() {
		var t = disp.getImageById( $(this).data("image-id") );
		
		disp.resetDisplay();
		disp.showImage(t);
		disp.showImageData(t);
	});
}

Display.prototype.addNewTarget = function(target) {
    this.targets.push(target);
}

Display.prototype.showImage = function(i){

    var s = this.stage;
	s.destroyChildren();
    var d = this;
    var local_targets = d.getTargetsByImageId(i.id); 
    var layer = new Kinetic.Layer(); 
    var target_layer = new Kinetic.Layer(); 
    var imObj = new Image(); 
    imObj.onload = function(){
        var l = new Kinetic.Image({
            x : 00,
            y : 00, 
            image : imObj, 
            width : 500,
            height : 400 
        }); 
        
        for(k in local_targets){
            //var yy = d.mapGeoToPixels(500, i.maxLat-i.minLat, local_targets[k].minLat-i.minLat); 
            //var xx = d.mapGeoToPixels(400, i.maxLon-i.minLon, local_targets[k].minLon-i.minLon); 
            //var h = d.mapGeoToPixels(500, i.maxLat-i.minLat, local_targets[k].maxLat) - d.mapGeoToPixels(500, i.maxLat-i.minLat, local_targets[k].minLat); 
            //var w = d.mapGeoToPixels(400, i.maxLon-i.minLon, local_targets[k].maxLon) - d.mapGeoToPixels(400, i.maxLon-i.minLon, local_targets[k].minLon); 

			// New value / old value
			var xPixelRatio = (500 / i.width);
			var yPixelRatio = (400 / i.height);
			
			var xx = xPixelRatio * local_targets[k].minLat;
			var yy = yPixelRatio  * local_targets[k].minLon;
			var w = xPixelRatio * local_targets[k].maxLat;
			var h = yPixelRatio * local_targets[k].maxLon;
			
			
            target_layer.add(
                new Kinetic.Rect({
                    x : xx,  
                    y : yy,  
                    width : w, 
                    height : h, 
                    stroke : local_targets[k].CVFound==1?"green":"blue", // CV found objects will be green?
                    strokeWidth : 4
                }) 
            ); 
			
			// Buttons are only shown for unverified images and targets. Otherwise only bounds are drawn.
			// Buttons will either verify a target permanently, or remove it
			if(i.stat === "NEW" && local_targets[k].status === "unverified")
			{
				var a = new Kinetic.Rect({
						x : xx - 24, 
						y : yy-4, 
						height : 20, 
						width : 20, 
						stroke : "green",
						strokewidth : 2, 
						fill : "green"
					}); 

				a.on("click", (function(tid, aid){
					return function() {
						var t = disp.getTargetById(tid);
						t.status = "verified";
						disp.showImage(disp.getImageById(aid));
					}
				}(local_targets[k].target, local_targets[k].imageId))); 
				// Closures are seriously black magic
				
				var r = new Kinetic.Rect({
						x : xx - 24, 
						y : yy+h-20, 
						height : 20, 
						width : 20, 
						stroke : "red",
						strokewidth : 2, 
						fill : "red"
					}); 

				r.on("click", (function(tid, aid){
					return function() {
						disp.removeTargetByTargetId(tid);
						disp.showImage(disp.getImageById(aid));
						
						var buffer = new PacketBuffer(10); // Opcode + 2 ints
					
						buffer.write(Packets["RejectImage"].opcode, buffer.VarType["ushort"]);
						
						// -1 for image means we're only removing a specific target
						buffer.write(-1, buffer.VarType["int"]);
						buffer.write(tid, buffer.VarType["int"]);

						buffer.send();
						delete buffer;
					}
				}(local_targets[k].target, local_targets[k].imageId)));

				target_layer.add(a); 
				target_layer.add(r);
			}
        }

        l.on("mousemove", function(){
            var mousePos = s.getPointerPosition();
            var x = mousePos.x;
            var y = mousePos.y;
            $("#mouse-x").html(x) 
            $("#mouse-y").html(y) 
            $("#mouse-geo-x").html(d.mapPixelsToGeo(500,i.maxLon - i.minLon, i.minLon, x)); 
            $("#mouse-geo-y").html(d.mapPixelsToGeo(400,i.maxLat - i.minLat, i.minLat, y)) 
        }); 

        layer.add(l); 
        s.add(layer); 
        s.add(target_layer); 
    }; 
    imObj.src = i.src; 
	
	this.displayID = i.id;
}

/**
* Displays the selected image, identified by @param:i, data
* on the data section of the screen, below the canvase
*/
Display.prototype.showImageData = function(i)
{
	// Update data associated to the buttons
	$("#verify-image").data("image-id", i.id);
	$("#reject-image").data("image-id", i.id);	
	
	// Refresh details table
	$("#details-table tbody").html("");
	
	var id = "<tr><td>ID: </td><td>"+i.id+"</td></tr>";
	var lon = "<tr><td>Center Latitude: </td><td>"+i.lat+"</td></tr>";
	var lat = "<tr><td>Center Longitude: </td><td>"+i.lon+"</td></tr>";
	
	$("#details-table tbody").append(id);
	$("#details-table tbody").append(lon);
	$("#details-table tbody").append(lat);
	
	// Get the list of targets
	var targets = this.getTargetsByImageId(i.id);
	
	//	Refresh target details
	$("#target-details").html("");
	
	var n;
	for(n = 0; n < targets.length; n++)
	{
		var data = jQuery.parseJSON(targets[n].description);
	
		$("#target-details").append("<h3>Target " + targets[n].target + "</h3>");

	
		$("#target-details").append("<div><ul><li>Shape: " + data.shape + "</li>" +
												"<li>Colour: " + data.colour + "</li></ul></div<");
	}
		
	// Because the accordion is pre-initialized, just have to refresh its display
	$( "#target-details" ).accordion( "refresh" );
}

Display.prototype.addTargetBoundingBox = function()
{
	var layer = new Kinetic.Layer(); 
	
	this.drag = new Kinetic.Rect({
			x : 17, 
			y : 17, 
			height : 25, 
			width : 25, 
			stroke : "yellow",
			strokewidth : 5,
			draggable: true
		}); 
	this.drag.strokeWidth(10);
	
	this.drag.edge = "none";
	
	this.box = new Kinetic.Rect({
			x : 20, 
			y : 20, 
			height : 20, 
			width : 20, 
			stroke : "blue",
			strokewidth : 2,
			draggable: true
		});
	
	this.drag.on("dragstart", (function(box, drag) {
	
		return function(evt) {
			var bx = box.getX();
			var by = box.getY();
			var cx = evt.evt.layerX;
			var cy = evt.evt.layerY;
			
			if(cx > (bx + box.getWidth()) && cy > by && cy < (by + box.getHeight()))
				drag.edge = "right";
			else if( cx < bx && cy > by && cy < (by + box.getHeight()))
				drag.edge = "left";
			else if(cy < by && cx > bx && cx < (bx + box.getWidth()))
				drag.edge = "top";
			else if(cy > (by + box.getHeight()) && cx > bx && cx < (bx + box.getWidth()))
				drag.edge = "bottom";
		}
	}(this.box, this.drag)));
	
	this.drag.on("dragmove", (function(box, drag){
		var prevEvt;
		
		return function(evt) {
			if(prevEvt == undefined) 
			{
				prevEvt = evt;
				return;
			}
			
			var bx = box.getX();
			var by = box.getY();
			var cx = evt.evt.layerX;
			var cy = evt.evt.layerY;

			if(drag.edge === "right") // Dragging right side
			{
				var change = evt.evt.layerX - bx - box.getWidth();
				
				box.setWidth( Math.max(box.getWidth() + change, 20));
				drag.setWidth( Math.max(drag.getWidth() + change, 25));
				
				drag.setX( bx - 3);
				drag.setY( by - 3);	
			}
			else if(drag.edge === "left")
			{
				var change = evt.evt.layerX - bx;
				
				box.setX( box.getX() + change);
				drag.setX( drag.getX() + change - 3);
				drag.setY( by - 3);	
				
				box.setWidth( Math.max(box.getWidth() - change, 20));
				drag.setWidth( Math.max(drag.getWidth() - change, 25));
			}
			else if(drag.edge === "bottom")
			{
				var change = evt.evt.layerY - by - box.getHeight();

				box.setHeight( Math.max(box.getHeight() + change, 20));
				drag.setHeight( Math.max(drag.getHeight() + change, 25));

				drag.setY( by - 3);
				drag.setX( bx - 3);
			}
			else if(drag.edge === "top")
			{
				var change = evt.evt.layerY - by;

				box.setHeight( Math.max(box.getHeight() - change, 20));
				drag.setHeight( Math.max(drag.getHeight() - change, 25));					
				
				box.setY( box.getY() + change);
				drag.setY( drag.getY() + change  - 3);
				drag.setX( bx - 3);
			}
			else
			{
				drag.setX( bx - 3);
				drag.setY( by - 3);						
			}
			
			prevEvt = evt;
		}
	}(this.box, this.drag)));
		
	this.box.on("dragmove", (function(box, drag){
		return function() {
			drag.setX( box.getX() - 3);
			drag.setY( box.getY() - 3);
		}
	}(this.box, this.drag))); 		

	layer.add(this.drag);
	layer.add(this.box);
	
	this.stage.add(layer);
}

Display.prototype.sendTargetToDatacenter = function(target)
{
	var buffer = new PacketBuffer(41 + target.description.length);
	
	// Here we write the opcode as a ushort
	buffer.write(Packets["ImageTarget"].opcode, buffer.VarType["ushort"]);
	
	// Write the length of our string as a ushort
	buffer.write(target.imageId, buffer.VarType["uint"]);
	buffer.write(target.target, buffer.VarType["uint"]);
	buffer.write(target.lat, buffer.VarType["uint"]);
	buffer.write(target.lon, buffer.VarType["uint"]);
	buffer.write(target.minLat, buffer.VarType["uint"]);
	buffer.write(target.minLon, buffer.VarType["uint"]);
	buffer.write(target.maxLat, buffer.VarType["uint"]);
	buffer.write(target.maxLon, buffer.VarType["uint"]);
	buffer.write(0, buffer.VarType["uchar"]);
	buffer.write(0, buffer.VarType["uchar"]);
	buffer.write(1, buffer.VarType["uchar"]);
	buffer.write(target.description.length, buffer.VarType["uint"]);
	buffer.write(target.description, buffer.VarType["uchar"], target.description.length);

	// Send our buffer
	buffer.send();
	
	// And clean up
	delete buffer;
}

/**
* Computes the location geographically of each pixel.
* Uses a ratio between the geographical size of the image and the pixel size of the image.
* Then multiples the ratio by the pixel number that is the input.
*
* @param pixel_size : the size of the image or container which will bound the geogrpahicall data.
* @param geo_size : the real life geographical size that the image or container representents. 
* @param geo_base : the base point of the container or image the corresponds to the 0 pixel location.
* @param input_pixel : the pixel number from within the container to compute the geographical loocation of.
*
* @return : the geographical location of the pixel that was input'd. 
*/
Display.prototype.mapPixelsToGeo = function(pixel_size, geo_size, geo_base, input_pixel){
    var ratio = geo_size/pixel_size; 
    return geo_base+(input_pixel*ratio); 
}


/**
* Convert a gps location to a pixel
*/
Display.prototype.mapGeoToPixels = function(pixel_size, geo_size, input_geo){
    var ratio = pixel_size/geo_size; 
    return Math.abs(input_geo*ratio);  
}

/**
* Refreshes all elements of the display. 
**/
Display.prototype.refresh = function(){
    this.refreshTable(); 
}

disp = new Display();

$("#verify-image").click( function() {
	var t = disp.getImageById( $(this).data("image-id") );
	if(t.stat === "NEW")
		t.stat = "VERIFIED";
	disp.resetDisplay();
	disp.showImage(t.id); // Force it to reflect new target bounds, if any
});

window.onkeyup = (function(event) {
    if (event.which == 65) { // key is a.
        var t = disp.getImageById( $("#verify-image").data("image-id") );
        if(t.stat === "NEW")
            t.stat = "VERIFIED";
        disp.refresh();
        disp.showImage(t.id); // Force it to reflect new target bounds, if any
    }else if(event.which == 82){ //key is r. 
        var t = disp.getImageById( $("#reject-image").data("image-id") );
        var i = disp.getIndexByImage(t);
        if(i != -1 && t.stat === "NEW")
        {
            disp.aerialImages.splice(i, 1);
            disp.refresh();
        }
    }
});



$("#reject-image").click( function() {
	var t = disp.getImageById( $(this).data("image-id") );
	var i = disp.getIndexByImage(t);
	if(i != -1 && t.stat === "NEW")
	{
		disp.aerialImages.splice(i, 1);
		disp.resetDisplay();
		
		var buffer = new PacketBuffer(10); // Opcode + 2 ints
	
		buffer.write(Packets["RejectImage"].opcode, buffer.VarType["ushort"]);
		
		// We specify just the image ID, telling the datacenter we want to delete the image + all target records
		buffer.write(t.id, buffer.VarType["int"]);
		buffer.write(-1, buffer.VarType["int"]);

		buffer.send();
		delete buffer;
	}
});

//=======================================
//END DISPLAY CLASS
//=======================================

//=======================================
//Target CLASS
//=======================================

/*
* This class embodies a target object in the database.  
* Built based on the package
*
* @param targetId: the id of the target in the databae
* @param imageId: id of the image the target resides in.
* @param lat: the latitude of the target.
* @param lon: the longitude of the target.
* @param boxMinX: the upper left corners x value of the target
* @param boxMinY: the upper left corners y value of the target
* @param boxMaxX: the bottom right corner x value of the target
* @param boxMaxY: the bottom right corner y value of the target
* @param description: a json string that contains the meta data about the image. 
*/
function Target(targetId, imageId, lat, lon, boxMinX, boxMinY, boxMaxX, boxMaxY, description, cvFound){
    this.target = targetId;
    this.imageId = imageId;
    this.lat = lat;
    this.lon = lon;
	this.minLat = boxMinX;
	this.minLon = boxMinY;
	this.maxLat = boxMaxX;
	this.maxLon = boxMaxY;
    this.description = description;
	this.status = "unverified";
	this.CVFound = cvFound;
}
//=======================================
//END TargetCLASS
//=======================================
//=======================================
//AerialImage CLASS
//=======================================

/*
* This class emboides the aerial image entity in the 
* systems database.  
*
* @param imageId: the id of hte image in the database
* @param lat: the latitude of the plane when the image was taken
* @param lon: the longitude of the plane when the image was taken
* @param minLat: the upper left corners latitude
* @param minLon: the upper left corners longitude
* @param maxLat: the lower right corners latitude
* @param maxLon: the lower right corners lon
* @param url: the url to access the image on the data center
* @param width: the width of the image 
* @param height: the heigh of the image 
*/

function AerialImage(imageId, lat, lon, minLat, minLon, maxLat, maxLon, url, width, height, src){
    this.id = imageId;
    this.lat = lat;
    this.lon = lon; 
    this.minLat = minLat; 
    this.minLon = minLon; 
    this.maxLat = maxLat; 
    this.maxLon = maxLon; 
    this.url = url;
    this.width = width;
    this.height = height; 
    this.stat = "NEW";
	this.src = src;
}
//=======================================
//END AerialImage Class
//=======================================


//======================================
//Event handlers input submission and sending
//======================================


//=======================================
//UI - Buttons & Status
//=======================================
$('#connect-btn').click( function() {

	if(connected == true)
		socket.close();
	else
		do_connect();
});

$('#connected').html("Connecting");
$('#connect-btn').html("Connecting");

//=======================================
//UI - Sliders
//=======================================
sendRecognitionValues = function() {
	// Packet is 0x107, and has length 2 + 1 * 6 (1 uchar for each value)
	var values = new PacketBuffer(8);
	
	// Write opcode
	values.write(Packets["RecognitionValues"].opcode, values.VarType["ushort"]);
	
	// Write each value
	values.write($( "#gaussian" ).slider("value"), values.VarType["uchar"]);
	values.write($( "#canny-low" ).slider("value"), values.VarType["uchar"]);
	values.write($( "#canny-high" ).slider("value"), values.VarType["uchar"]);
	values.write($( "#hough-vote" ).slider("value"), values.VarType["uchar"]);
	values.write($( "#hough-length" ).slider("value"), values.VarType["uchar"]);
	values.write($( "#hough-distance" ).slider("value"), values.VarType["uchar"]);
	
	values.send();

	delete values;
};
	
$(document).ready(function() {
	$( "#gaussian" ).slider({
		min: 1,
		max: 55,
		step: 1,
		slide: function( event, ui ) {
			$( "#gaussian-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});
	
	$( "#canny-low" ).slider({
		min: 1,
		max: 255,
		step: 1,
		slide: function( event, ui ) {
			$( "#canny-low-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});
	
	$( "#canny-high" ).slider({
		min: 1,
		max: 255,
		step: 1,
		slide: function( event, ui ) {
			$( "#canny-high-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});

	$( "#hough-vote" ).slider({
		min: 1,
		max: 255,
		step: 1,
		slide: function( event, ui ) {
			$( "#hough-vote-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});

	$( "#hough-length" ).slider({
		min: 0,
		max: 255,
		step: 1,
		slide: function( event, ui ) {
			$( "#hough-length-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});

	$( "#hough-distance" ).slider({
		min: 0,
		max: 255,
		step: 1,
		slide: function( event, ui ) {
			$( "#hough-distance-val" ).html( ui.value );
		},
		stop: function(event, ui) {
			sendRecognitionValues();
		}
	});
	
	$("#add-new-target").click( function() {
		disp.addTargetBoundingBox();
		
		$("#add-target-shape").removeClass("input-disabled");
		$("#add-target-colour").removeClass("input-disabled");
		$("#add-new-target-confirm").removeClass("input-disabled");
	});
	
	$("#add-new-target-confirm").click( function() {
		if(disp.displayID >= 0)
		{
			var i = disp.getImageById(disp.displayID);
			
			var shape = $("#add-target-shape").val();
			var colour = $("#add-target-colour").val();
			
			var lon = disp.mapPixelsToGeo(500, i.maxLon - i.minLon, i.minLon, (disp.box.getX() + disp.box.getWidth()) / 2);
            var lat = disp.mapPixelsToGeo(400, i.maxLat - i.minLat, i.minLat, (disp.box.getY() + disp.box.getHeight()) / 2);
			
			var json = "{ shape : " + shape + ", primaryColour : " + colour + " }";
			
			var xPixelRatio = (i.width / 500);
			var yPixelRatio = (i.height / 400);
			
			var minX = disp.box.getX();
			var minY = disp.box.getY();
			var maxX = disp.box.getY() + disp.box.getHeight();
			var maxY = disp.box.getX() + disp.box.getWidth();

			
			var t = new Target(0, disp.displayID, lat, lon, minX*xPixelRatio, minY*yPixelRatio, maxX*xPixelRatio, maxY*yPixelRatio, json, false);
			
			disp.sendTargetToDatacenter(t);

			disp.showImage(i);
		}
		
		$("#add-target-shape").addClass("input-disabled");
		$("#add-target-colour").addClass("input-disabled");
		$("#add-new-target-confirm").addClass("input-disabled");
	});
});

//=======================================
// Run functions
//=======================================

do_connect();
