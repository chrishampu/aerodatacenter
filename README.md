# This project has been moved to the UVIc AERO Team Repository: https://bitbucket.org/uvicaero/datacenter/overview #



**README **

UVic AERO Datacenter Project

This project handles all intercommunication between UAS systems such as the web clients, antenna tracker, onboard computer and UAV.
It is also responsible for image processing, black box logging, report generation, interfacing with web clients, serving files, and database management.

### What is this repository for? ###

* Acting as a central host for the Datacenter project
* A medium of collaboration between AERO members

### How do I get set up? ###

* The project supports Linux & Windows compilation
* Make sure you download the extra header files in the downloads section
* Compile on Windows using Visual Studio 2013
* Compile on Linux by running the following commands
[code]
sudo sh autogen.sh
sudo ./configure
sudo make
[/code]

### Contribution guidelines ###

* Request code reviews from Chris when you are unsure about something
* Follow C++ coding guidelines: https://google-styleguide.googlecode.com/svn/trunk/cppguide.xml

### Who do I talk to? ###

* Chris at chrishampu@outlook.com for general inquiries, code review, or requesting write access
* Simon at simon.diemert@gmail.com who is also very knowledgeable about datacenter operation